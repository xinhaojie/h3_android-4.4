PRODUCT_PACKAGES += \
	busybox \
	busybox-smp \
	fsck.exfat \
	iostat \
	mkfs.exfat \
	mkntfs \
	mount.exfat \
	ntfs-3g \
	ntfs-3g.probe \
	data_resume.sh \
	preinstall.sh \
	flushcache.sh \
	logger.sh \
	mke2fs.ext4 \
	nfsprobe \
	mtop \
	memtester \
	cpu_monitor \
    log_bg.sh
