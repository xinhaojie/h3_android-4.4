package com.cmcc.media;

/**
 * 遥控器KeyCode定义
 */
public interface RemoteControl {
	/**
	 * 遥控器“切歌”按钮KeyCode
	 */
	public static final int KEYCODE_CMCC_SWITCH_SONG = 131;
	/**
	 * 遥控器“导唱”按钮KeyCode
	 */
	public static final int KEYCODE_CMCC_GUIDE_MELODY = 132;
	/**
	 * 遥控器“调音台”按钮KeyCode
	 */
	public static final int KEYCODE_CMCC_SOUND_CONSOLE = 133;
	/**
	 * 遥控器“控制台”按钮KeyCode
	 */
	public static final int KEYCODE_CMCC_CONSOLE = 134;
	/**
	 * 遥控器“下一曲”按钮KeyCode
	 */
	public static final int KEYCODE_CMCC_MEDIA_NEXT = 135;
	/**
	 * 遥控器“上一曲”按钮KeyCode
	 */
	public static final int KEYCODE_CMCC_MEDIA_PREVIOUS = 136;
	/**
	 * 遥控器“播放暂停”按钮KeyCode
	 */
	public static final int KEYCODE_CMCC_MEDIA_PLAY_PAUSE = 137;
	/**
	 * 遥控器“麦克风音量加”按钮KeyCode
	 */
	public static final int KEYCODE_CMCC_MIC_VOLUME_UP = 138;
	/**
	 * 遥控器“麦克风音量减”按钮KeyCode
	 */
	public static final int KEYCODE_CMCC_MIC_VOLUME_DOWN = 139;

}
