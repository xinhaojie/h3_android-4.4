#ifndef PRI_CONFIG_H
#define PRI_CONFIG_H

#define SW_JAWS 0x00
#define SW_EAGLE 0x02
#define SW_DOLPHIN 0x03

#define PLATFORM_CMCCWASU	0x08
#define PLATFORM_ALIYUN		0x09
#define PLATFORM_TVD		0x0A
#define UNKNOWN_PLATFORM	0xFF

#ifdef USE_SW_DEINTERLACE
	#undef USE_SW_DEINTERLACE
#endif

#if(SW_CHIP_PLATFORM == SW_JAWS)
	#define CONFIG_CHIP    OPTION_CHIP_1639
#elif (SW_CHIP_PLATFORM == SW_EAGLE)
	#define CONFIG_CHIP    OPTION_CHIP_1673
	#define USE_SW_DEINTERLACE	//just define for git
#elif (SW_CHIP_PLATFORM == SW_DOLPHIN)
	#define CONFIG_CHIP    OPTION_CHIP_1680
#else
	#error "please select a platform\n"
#endif

#if(BUSINESS_PLATFORM == PLATFORM_CMCCWASU)
	#define CONFIG_CMCC    OPTION_CMCC_YES
#endif

#define KEEP_LAST_FRAME_FLAG 0
#endif
