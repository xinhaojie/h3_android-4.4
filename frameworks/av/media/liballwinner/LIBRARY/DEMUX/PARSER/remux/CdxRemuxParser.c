#include <CdxTypes.h>
#include <CdxLog.h>
#include <CedarXRtspSpec.h>
#include <CdxStream.h>
#include <CdxMemory.h>
#include <CdxParser.h>
#include <AwRtpStream.h>

#define RTSP_CACHE_TIME (10*1000000) //10s

//#define SAVE_STREAM

#ifdef SAVE_STREAM
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

static int streamFd = -1;

static void CdxStreamSave(void *buf, cdx_uint32 len)
{

    if (streamFd == -1)
    {
        unlink("/mnt/sdcard/debug_stream");
        streamFd = open("/mnt/sdcard/debug_stream", O_CREAT, 00666);
        if (streamFd == -1)
        {
            CDX_LOGE("fopen failure, errno(%d)", errno);
        }
    }
    write(streamFd, buf, len);
    fsync(streamFd);

    return;
//    CDX_LOGD("%s,%u", __FUNCTION__, len);
}
#endif

static cdx_void GetMediaInfo(CdxMediaInfoT *info, struct CedarXRtspMetaS *meta)
{
    memset(info, 0x00, sizeof(*info));

    info->fileSize = 0;
    info->bSeekable = CDX_FAILURE;

    info->programNum = 0;
    info->programIndex = 0;

    info->program[0].id = 0;
    info->program[0].flags = 0;
    info->program[0].duration = 0;
    
    info->program[0].subtitleNum = 0;
    info->program[0].subtitleIndex = -1;
    
    if (meta->header.hasAudio)
    {
        info->program[0].audioNum = 1;
        info->program[0].audioIndex = 0;

        info->program[0].audio[0].eCodecFormat = meta->audioRegion.codingType;
        info->program[0].audio[0].eSubCodecFormat = meta->audioRegion.subCodingType;

        info->program[0].audio[0].nChannelNum = meta->audioRegion.channels;
        info->program[0].audio[0].nSampleRate = meta->audioRegion.samplePerSecond;

        if (meta->audioRegion.extraDataLen)
        {
            info->program[0].audio[0].nFlags = 0;
            info->program[0].audio[0].nCodecSpecificDataLen = meta->audioRegion.extraDataLen;
            info->program[0].audio[0].pCodecSpecificData = meta->audioExtraData;
        }
        else
        {
            info->program[0].audio[0].nFlags = 1;
        }
    }
    
    if (meta->header.hasVideo)
    {
        info->program[0].videoNum = 1;
        info->program[0].videoIndex = 0;
        
        info->program[0].video[0].eCodecFormat = meta->videoRegion.codingType;
        info->program[0].video[0].nWidth = meta->videoRegion.width;
        info->program[0].video[0].nHeight = meta->videoRegion.height;
        
        info->program[0].video[0].nFrameRate = 0;
        info->program[0].video[0].nFrameDuration = 0;
        info->program[0].video[0].nAspectRatio = 0;
        
        info->program[0].video[0].bIs3DStream = 0;
        info->program[0].video[0].nCodecSpecificDataLen = meta->videoRegion.extraDataLen;
        info->program[0].video[0].pCodecSpecificData = meta->videoExtraData;
    }

    return ;
}

struct CdxRemuxParserImplS
{
    CdxParserT base;
    CdxStreamT *stream;
    struct CedarXRtspMetaS meta;
};



cdx_int32 __RemuxPsrSeekTo(CdxParserT *parser, cdx_int64 timeUs)
{
    (void)parser;
    (void)timeUs;
    CDX_LOGE("not support...");
    return -1;
}

cdx_uint32 __RemuxPsrAttribute(CdxParserT *parser) /*return falgs define as open's falgs*/
{
    (void)parser;
    return 0;
}


cdx_int32 __RemuxPsrGetStatus(CdxParserT *parser) /*return enum CdxPrserStatusE*/
{
    (void)parser;
    return PSR_OK;
}


cdx_int32 __RemuxPsrControl(CdxParserT *parser, int cmd, void *param)
{
    struct CdxRemuxParserImplS *impl;

    impl = CdxContainerOf(parser, struct CdxRemuxParserImplS, base);

    switch(cmd)
    {
    case CDX_PSR_CMD_GET_CACHESTATE:
    {
        int ret;
        struct ParserCacheStateS *psrCS = param;
        struct StreamCacheStateS stCS;
        ret = CdxStreamControl(impl->stream, STREAM_CMD_GET_CACHESTATE, &stCS);
        if (ret != CDX_SUCCESS)
        {
            CDX_LOGE("get cache state failure, err(%d)", ret);
            return -1;
        }
        psrCS->nCacheCapacity = stCS.nCacheCapacity;
        psrCS->nBandwidthKbps = stCS.nBandwidthKbps;
        psrCS->nCacheSize = stCS.nCacheSize;
        psrCS->nCacheCapacity = stCS.nCacheCapacity;
        psrCS->nPercentage = stCS.nPercentage;
        return 0;
    }    
    default:
        CDX_LOGW("cmd(%d) not implment now...", cmd);
        break;
    }
    
    return CDX_SUCCESS;
}

cdx_int32 __RemuxPsrPrefetch(CdxParserT *parser, CdxPacketT *pkt)
{
    struct CdxRemuxParserImplS *impl;
    cdx_int32 ret;
    
    struct CedarXRtspPktHeadS ptkHead;

    impl = CdxContainerOf(parser, struct CdxRemuxParserImplS, base);

    memset(pkt, 0x00, sizeof(*pkt));
    
    ret = CdxStreamRead(impl->stream, &ptkHead, sizeof(ptkHead));
    if (sizeof(ptkHead) != ret)
    {
        CDX_LOGE("read failure..., req(%d), ret(%d)", sizeof(ptkHead), ret);
        return CDX_FAILURE;
        
    }

    pkt->type = ptkHead.type;
    pkt->length = ptkHead.lenght;
    pkt->pts = ptkHead.pts;// * 1000;
    pkt->flags |= (FIRST_PART|LAST_PART);
    pkt->streamIndex = 0;

    if (pkt->type != CDX_MEDIA_AUDIO && pkt->type != CDX_MEDIA_VIDEO)
    {        
        CDX_LOGD("pkt type(%d), length(%d)", pkt->type, pkt->length);
        
        memset(pkt, 0x00, sizeof(*pkt));
        return CDX_SUCCESS;
    }

//    if ((((cdx_uint32)pkt->pts) & (0xf << 19)) == 0)
//    {
//        CDX_LOGD("remux psr get pkt type(%d), pts(%.3f)", pkt->type, pkt->pts/1000000.0);
//    }
    
    return CDX_SUCCESS;
    
}

cdx_int32 __RemuxPsrRead(CdxParserT *parser, CdxPacketT *pkt)
{
    cdx_int32 ret;
    struct CdxRemuxParserImplS *impl;

    impl = CdxContainerOf(parser, struct CdxRemuxParserImplS, base);
    
    if (pkt->buflen >= pkt->length)
    {
        ret = CdxStreamRead(impl->stream, pkt->buf, pkt->length);
        if (ret != pkt->length)
        {
            CDX_LOGE("read stream error.");
            return CDX_FAILURE;
        }
        
#ifdef SAVE_STREAM
        if (pkt->type == CDX_MEDIA_VIDEO)
        {
            CdxStreamSave(pkt->buf, pkt->length);
        }
#endif
        
    }
    else 
    {
        CDX_LOG_CHECK(pkt->buflen + pkt->ringBufLen >= pkt->length, 
            "buffer not enough (%d,%d,%d)", pkt->buflen, pkt->ringBufLen, pkt->length);
            
        ret = CdxStreamRead(impl->stream, pkt->buf, pkt->buflen);
        if (ret != pkt->buflen)
        {
            CDX_LOGE("read stream error.");
            return CDX_FAILURE;
        }
        
#ifdef SAVE_STREAM
        if (pkt->type == CDX_MEDIA_VIDEO)
        {
            CdxStreamSave(pkt->buf, pkt->buflen);
        }
#endif

        ret = CdxStreamRead(impl->stream, pkt->ringBuf, pkt->length - pkt->buflen);
        if (ret != pkt->length - pkt->buflen)
        {
            CDX_LOGE("read stream error.");
            return CDX_FAILURE;
        }
        
#ifdef SAVE_STREAM
        if (pkt->type == CDX_MEDIA_VIDEO)
        {
            CdxStreamSave(pkt->ringBuf, pkt->length - pkt->buflen);
        }
#endif
    }

    return CDX_SUCCESS;
}

cdx_int32 __RemuxPsrGetMediaInfo(CdxParserT *parser, CdxMediaInfoT *info)
{
    struct CdxRemuxParserImplS *impl;

    impl = CdxContainerOf(parser, struct CdxRemuxParserImplS, base);

    GetMediaInfo(info, &impl->meta);

    return CDX_SUCCESS;
}

cdx_int32 __RemuxPsrClose(CdxParserT *parser)
{
    struct CdxRemuxParserImplS *impl;

    impl = CdxContainerOf(parser, struct CdxRemuxParserImplS, base);

    CdxStreamClose(impl->stream);

#ifdef SAVE_STREAM
    if (streamFd != -1)
    {
        close(streamFd);
        streamFd = -1;
    }
#endif

    return CDX_SUCCESS;
}

static int __RemuxPsrInit(CdxParserT *parser)
{
    struct CdxRemuxParserImplS *impl;
    cdx_int32 ret, count;

    impl = CdxContainerOf(parser, struct CdxRemuxParserImplS, base);
    
    count = sizeof(struct CedarXRtspHeaderS);
    ret = CdxStreamRead(impl->stream, &impl->meta.header, count);
    if (ret != count)
    {
        CDX_LOGE("something in stream error!");
        goto failure;
    }
     
    if (impl->meta.header.hasVideo)
    {
        count = sizeof(struct CedarXRtspVideoRegionS);
        ret = CdxStreamRead(impl->stream, &impl->meta.videoRegion, count);
        if (ret != count)
        {
            CDX_LOGE("something in stream error!");
            goto failure;
        }

        CDX_LOGD("videoRegion extraDataLen: %u", impl->meta.videoRegion.extraDataLen);
        if (impl->meta.videoRegion.extraDataLen > 0)
        {
            count = impl->meta.videoRegion.extraDataLen;
            impl->meta.videoExtraData = (cdx_char *)CdxMalloc(count);
            CDX_FORCE_CHECK(impl->meta.videoExtraData);
            
            ret = CdxStreamRead(impl->stream, impl->meta.videoExtraData, count);
            if (ret != count)
            {
                CDX_LOGE("something in stream error!");
                goto failure;
            }
#ifdef SAVE_STREAM
            CDX_BUF_DUMP(impl->meta.videoExtraData, count);
            CdxStreamSave(impl->meta.videoExtraData, count);
#endif
        }
    }
    
    if (impl->meta.header.hasAudio)
    {
        count = sizeof(struct CedarXRtspAudioRegionS);
        ret = CdxStreamRead(impl->stream, &impl->meta.audioRegion, count);
        if (ret != count)
        {
            CDX_LOGE("something in stream error!");
            goto failure;
        }
        CDX_LOGD("audioRegion extraDataLen: %u", impl->meta.audioRegion.extraDataLen);
        if (impl->meta.audioRegion.extraDataLen > 0)
        {
            count = impl->meta.audioRegion.extraDataLen;
            impl->meta.audioExtraData = (cdx_char *)CdxMalloc(count); 
            if (!impl->meta.audioExtraData)
            {
                CDX_LOGE("malloc fail, size:%u", count);
                goto failure;
            }
            ret = CdxStreamRead(impl->stream, impl->meta.audioExtraData, count);
            if (ret != count)
            {
                CDX_LOGE("something in stream error!");
                goto failure;
            }
        }
    }

    return 0;
    
failure:
    if (impl->meta.videoExtraData)
    {
        CdxFree(impl->meta.videoExtraData);
    }
    if (impl->meta.audioExtraData)
    {
        CdxFree(impl->meta.audioExtraData);
    }

    return -1;
}

struct CdxParserOpsS remuxParserOps =
{
    .control        = __RemuxPsrControl,
    .prefetch       = __RemuxPsrPrefetch,
    .read           = __RemuxPsrRead,
    .getMediaInfo   = __RemuxPsrGetMediaInfo,
    .seekTo         = __RemuxPsrSeekTo,
    .attribute      = __RemuxPsrAttribute,
    .getStatus      = __RemuxPsrGetStatus,
    .close          = __RemuxPsrClose,
    .init           = __RemuxPsrInit,
};

static CdxParserT *__RemuxPsrCreate(CdxStreamT *stream, cdx_uint32 flags)
{
    (void)flags;
    struct CdxRemuxParserImplS *impl = NULL;

    impl = CdxMalloc(sizeof(*impl));
    CDX_FORCE_CHECK(impl);
    memset(impl, 0x00, sizeof(*impl));

    impl->base.ops = &remuxParserOps;
    impl->stream = stream;
    
    return &impl->base;
}

static cdx_uint32 __RemuxPsrProbe(CdxStreamProbeDataT *probeData)
{
    if (!probeData || probeData->len < 8 || memcmp(probeData->buf, "remu", 4))
    {
        CDX_LOGW("not privite remux file...");
        return 0;
    }

    CDX_LOGI("privite remux file...");
    return 100;
}


CdxParserCreatorT remuxParserCtor =
{
    .create = __RemuxPsrCreate,
    .probe = __RemuxPsrProbe
};

