/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_NDEBUG 0
#define LOG_TAG "AVorbisAssembler"
#include <utils/Log.h>
#include <CdxLog.h>

#include "AVorbisAssembler.h"

#include "ARTPSource.h"
#include "ASessionDescription.h"

#include <media/stagefright/foundation/ABitReader.h>
#include <media/stagefright/foundation/ABuffer.h>
#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/foundation/hexdump.h>
#include <media/stagefright/foundation/base64.h>
#include <media/stagefright/Utils.h>

#include <ctype.h>
#include <stdint.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

namespace android {


static uint16_t u16at(const uint8_t *data) {
    return data[0] << 8 | data[1];
}
static uint32_t u24at(const uint8_t *data) {
    return data[0] << 16 | u16at(&data[1]);
}

static uint32_t u32at(const uint8_t *data) {
    return u16at(data) << 16 | u16at(&data[2]);
}

bool AVorbisAssembler::isAdjustLengthNeccessary(const sp<ABuffer> &buffer)
{
	const uint8_t *data = buffer->data();
	uint8_t fragType   = (data[3] & 0xc0) >> 6;
	uint8_t packetsNum = data[3] & 0x0f;

	if(packetsNum == 1 ) {
		return true;
	}

	if(fragType != NotFragmented) {
		CHECK(packetsNum == 0);
		return true;
	}

	return false;
}


int32_t AVorbisAssembler::getPacketLength(const sp<ABuffer> &buffer)
{
	int32_t nalLen 	= 0;
	size_t off		= 0;
	uint32_t len	= 0;
	off += 4;

	bool adjustLen = isAdjustLengthNeccessary(buffer);

	const uint8_t *data = buffer->data();

	size_t size = buffer->size();
	while(off < size) {
		len = u16at(&data[off]);
		if(adjustLen && (len != size - 4)) {
			len = size - 4;
		} else {
			//length flag.
			off += 2;
		}
		//data len;
		nalLen += len;
		//lace fill len;
		nalLen += (len /0x100 + 1);
		//header len;
		nalLen += 27;

		//data part
		off += len;
	}
	ALOGD("nal len %d, len %d", nalLen, len);

	return nalLen;
}

int32_t AVorbisAssembler::splitXiphHeaders(const sp<ABuffer> &codecData, sp<ABuffer> *configuration)
{
	uint8_t  *headerStart[3];
    uint32_t headerLen[3];
    int32_t  i;
    uint32_t  overAllLen;
    uint8_t  *oggHeader;
    uint8_t  lacingFill, lacingFill_0, lacingFill_1, lacingFill_2;

    int32_t configLen;
	

	uint8_t *codecPriv = codecData->data();
	uint32_t codecPrivSize = codecData->size();

	if(codecPrivSize <= 8) {
		return UNKNOWN_ERROR;
	}

	uint32_t offset = 0;
	uint32_t ident  = u24at(&codecPriv[0]);
	uint16_t len    = u16at(&codecPriv[3]);

	offset += 5;

	codecPriv += 5;
	codecPrivSize -= 5;

	offset += len;


    if (codecPrivSize >= 6 && u16at(&codecPriv[0]) == 30) {
    	overAllLen = 6;
        for (i=0; i<3; i++) {
            headerLen[i] = u16at(&codecPriv[0]);
            codecPriv += 2;
            offset	+= 2;
            headerStart[i] = codecPriv;
            codecPriv += headerLen[i];
            offset += headerLen[i];
            if (overAllLen > codecPrivSize - headerLen[i])
                return UNKNOWN_ERROR;
            overAllLen += headerLen[i];
        }
    } else if (codecPrivSize >= 3 && codecPriv[0] == 2) {
        overAllLen = 3;
        codecPriv++;
        offset ++;
        for (i=0; i<2; i++, codecPriv++, offset++) {
            headerLen[i] = 0;
            for (; (overAllLen < codecPrivSize) && (*codecPriv == 0xff); codecPriv++) {
                headerLen[i] += 0xff;
                overAllLen   += 0xff + 1;
            }
            headerLen[i] += *codecPriv;
            overAllLen   += *codecPriv;
            if (overAllLen > codecPrivSize)
                return UNKNOWN_ERROR;
        }
        headerLen[2] = codecPrivSize - overAllLen;
        headerStart[0] = codecPriv;
        headerStart[1] = headerStart[0] + headerLen[0];
        headerStart[2] = headerStart[1] + headerLen[1];
    } else {
        return UNKNOWN_ERROR;
    }

    for(lacingFill_0 = 0; lacingFill_0*0xff < headerLen[0]; lacingFill_0++);
    for(lacingFill_1 = 0; lacingFill_1*0xff < headerLen[1]; lacingFill_1++);
    for(lacingFill_2 = 0; lacingFill_2*0xff < headerLen[2]; lacingFill_2++);

    configLen =  27 + lacingFill_0 + headerLen[0]
    	       		+  27 + lacingFill_1 + headerLen[1]
        	    		  + lacingFill_2 + headerLen[2];

    configuration->clear();
    *configuration = new ABuffer(configLen);

    //set ident data.
    (*configuration)->setInt32Data(ident);
    oggHeader = (*configuration)->data();

    for(i = 0; i < 26; i++)
    {
       oggHeader[i] = 0;
    }

    oggHeader[0] = 0x4f;
    oggHeader[1] = 0x67;
    oggHeader[2] = 0x67;
    oggHeader[3] = 0x53;
    oggHeader[5] = 0x2;
    lacingFill 	 = lacingFill_0;
    oggHeader[26] = lacingFill;
    oggHeader += 27;
    for(i = 0; i < lacingFill; i++)
    {
    	if(i != (lacingFill-1))
    	{
    		oggHeader[i] = 0xff;
    	}
    	else
    	{
    		oggHeader[i] = headerLen[0] - (lacingFill-1)*0xff;
    	}
    }
    oggHeader += lacingFill;

    memcpy(oggHeader, headerStart[0], headerLen[0]);
    oggHeader += headerLen[0];

    for(i = 0; i < 26; i++)
    {
       oggHeader[i] = 0;
    }
    oggHeader[0] = 0x4f;
    oggHeader[1] = 0x67;
    oggHeader[2] = 0x67;
    oggHeader[3] = 0x53;
    oggHeader[18] = 0x1;
    oggHeader[26] = lacingFill_1 + lacingFill_2;
    oggHeader += 27;
    lacingFill = lacingFill_1;
    for(i = 0; i < lacingFill; i++)
    {
    	if(i != (lacingFill-1))
    	{
    		oggHeader[i] = 0xff;
    	}
    	else
    	{
    		oggHeader[i] = headerLen[1] - (lacingFill-1)*0xff;
    	}
    }
    oggHeader += lacingFill;

    lacingFill = lacingFill_2;
    for(i = 0; i < lacingFill; i++)
    {
    	if(i != (lacingFill-1))
    	{
    		oggHeader[i] = 0xff;
    	}
    	else
    	{
    		oggHeader[i] = headerLen[2] - lacingFill*0xff - 1;
    	}
    }
    oggHeader += lacingFill;

    memcpy(oggHeader, headerStart[1], headerLen[1]);
    oggHeader += headerLen[1];
    memcpy(oggHeader, headerStart[2], headerLen[2]);
    oggHeader += headerLen[2];

    CHECK_LE(codecData->offset() + offset, codecData->capacity());
    CHECK_LE(offset, codecData->size());
    codecData->setRange(codecData->offset() + offset, codecData->size() - offset);

    return 0;
}


// static
AVorbisAssembler::AVorbisAssembler(
        const sp<AMessage> &notify, const AString &desc, const AString &params)
    : mNotifyMsg(notify),
      mParams(params),
      mAccessUnitRTPTime(0),
      mNextExpectedSeqNoValid(false),
      mNextExpectedSeqNo(0),
      mCurrentIdent(0),
      mAccessUnitDamaged(false),
      mAddPackedHeader(false),
      mSubmitConfiguration(false),
      mPageNum(0),
      mHeaderType(0) {
    CDX_UNUSE(desc);
	AString item = "configuration=";
	ssize_t startPos = mParams.find(item.c_str(), 0);
	if(startPos >= 0) {
		AString itemValue;
		startPos += item.size();
		ssize_t endPos = mParams.find(";", startPos);
		if(endPos < 0) {
			endPos = mParams.size();
		}
#if 0
		ALOGV("mParams size %u, startPos %d, endPos %d", mParams.size(), startPos, endPos);
#endif
		itemValue.setTo(mParams, startPos, endPos - startPos);
		sp<ABuffer> headers = decodeBase64(itemValue);
		parsePackedHeaders(headers);
	}

#if 0
		int fd = open("/data/camera/config.dat", O_CREAT | O_RDWR, 644);
		if(fd >= 0) {
			List<sp<ABuffer> >::iterator it = mConfigs.begin();
			ALOGD("(*it) %p", (*it).get());
			List<sp<ABuffer> >::iterator end = mConfigs.begin();
			ALOGD("(*end) %p", (*end).get());
			while(it != mConfigs.end()) {
				write(fd, (*it)->data(), (*it)->size());

				ALOGD("xx(*it) %p, size %u", (*it).get(), (*it)->size());
				++it;
			}
			close(fd);
		}
#endif
}

AVorbisAssembler::~AVorbisAssembler() {
}

ARTPAssembler::AssemblyStatus AVorbisAssembler::addPacket(
        const sp<ARTPSource> &source) {
    List<sp<ABuffer> > *queue = source->queue();

    if (queue->empty()) {
        return NOT_ENOUGH_DATA;
    }

    if (mNextExpectedSeqNoValid) {
        List<sp<ABuffer> >::iterator it = queue->begin();
        while (it != queue->end()) {
            if ((uint32_t)(*it)->int32Data() >= mNextExpectedSeqNo) {
                break;
            }

            it = queue->erase(it);
        }

        if (queue->empty()) {
            return NOT_ENOUGH_DATA;
        }
    }

    sp<ABuffer> buffer = *queue->begin();

    if (!mNextExpectedSeqNoValid) {
        mNextExpectedSeqNoValid = true;
        mNextExpectedSeqNo = (uint32_t)buffer->int32Data();
    } else if ((uint32_t)buffer->int32Data() != mNextExpectedSeqNo) {
        ALOGV("Not the sequence number I expected");

        return WRONG_SEQUENCE_NUMBER;
    }

    uint32_t rtpTime;
    CHECK(buffer->meta()->findInt32("rtp-time", (int32_t *)&rtpTime));

    if ((mPackets.size() > 0 && rtpTime != mAccessUnitRTPTime)
    		|| mSubmitConfiguration
    		|| mAddPackedHeader) {
        submitAccessUnit();

        mSubmitConfiguration = false;
    }
    mAccessUnitRTPTime = rtpTime;

    /*
     * Vorbis Payload Header
     *
     *  0                   1                   2                   3
     *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     * |                Ident                            | F |VDT|# pkts.|
     * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     *
     */


    const uint8_t *data = buffer->data();

    //4 bytes payload header.
    CHECK_GE(buffer->size(), 4);

    uint32_t ident 		  = u24at(&data[0]);
    uint32_t dataType 	  = (data[3] & 0x30) >> 4;

    if(ident != mCurrentIdent) {
    	ALOGI("new ident %x, current %d", ident, mCurrentIdent);
    	mCurrentIdent = ident;
    	CHECK(mAddPackedHeader != true);
    	mAddPackedHeader = true;
    }

    const uint8_t *tmp = data;
	ALOGD("%02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x",
	tmp[0],tmp[1],tmp[2],tmp[3],
	tmp[4],tmp[5],tmp[6],tmp[7],
	tmp[8],tmp[9],tmp[10],tmp[11],
	tmp[12],tmp[13],tmp[14],tmp[15]);

	ALOGV("data type %d", dataType);
    switch(dataType) {
    case RawVorbisPayload:
    	break;

    case PackedConfigurationPayload:
    	mSubmitConfiguration = true;
    	break;

    case LegacyCommentPayload:
    	//ignore comment.
    default:
        queue->erase(queue->begin());
        ++mNextExpectedSeqNo;
        return OK;
    }
    mPackets.push_back(buffer);

    queue->erase(queue->begin());
    ++mNextExpectedSeqNo;
    return OK;
}

void AVorbisAssembler::submitAccessUnit() {
    CHECK(!mPackets.empty());

    ALOGV("Access unit complete (%d nal units)", mPackets.size());

    size_t totalSize = 0;

    for (List<sp<ABuffer> >::iterator it = mPackets.begin();
         it != mPackets.end(); ++it) {
    	totalSize += getPacketLength(*it);
    }

    sp<ABuffer> header;
    if(mAddPackedHeader) {
    	CHECK_GE(mPackets.size(), 1);
    	header = getPackedHeaders(mCurrentIdent);
    	totalSize += header->size();
    }

    sp<ABuffer> accessUnit = new ABuffer(totalSize);
    accessUnit->setRange(0, 0);

    if(mAddPackedHeader && header->size()) {
    	memcpy(accessUnit->data(), header->data(), header->size());
    	accessUnit->setRange(0, header->size());
    	mAddPackedHeader = false;
    }

    for (List<sp<ABuffer> >::iterator it = mPackets.begin();
         it != mPackets.end(); ++it) {
        sp<ABuffer> nal = *it;
        parseBufferInfo(nal, accessUnit);
    }

    CopyTimes(accessUnit, *mPackets.begin());

    if (mAccessUnitDamaged) {
        accessUnit->meta()->setInt32("damaged", true);
    }

    mPackets.clear();
    mAccessUnitDamaged = false;

    sp<AMessage> msg = mNotifyMsg->dup();
    msg->setBuffer("access-unit", accessUnit);
    msg->post();
}

ARTPAssembler::AssemblyStatus AVorbisAssembler::assembleMore(
        const sp<ARTPSource> &source) {
    AssemblyStatus status = addPacket(source);
    if (status == MALFORMED_PACKET) {
        mAccessUnitDamaged = true;
    }
    return status;
}

void AVorbisAssembler::packetLost() {
    CHECK(mNextExpectedSeqNoValid);
    ALOGV("packetLost (expected %d)", mNextExpectedSeqNo);

    ++mNextExpectedSeqNo;

    mAccessUnitDamaged = true;
}

void AVorbisAssembler::onByeReceived() {
    sp<AMessage> msg = mNotifyMsg->dup();
    msg->setInt32("eos", true);
    msg->post();
}

void AVorbisAssembler::parsePackedHeaders(const sp<ABuffer> &headers)
{
	if(headers->size() <= 4) {
		return ;
	}

	uint32_t offset = 0;
	const uint8_t *data = headers->data();

	uint32_t headerNum = u32at(&data[0]);
	if(headerNum == 0) {
		return;
	}

	offset += 4;

	headers->setRange(headers->offset() + offset, headers->size() - offset);

	for(uint32_t i = 0; i < headerNum; i++) {
		sp<ABuffer> config;

		if(!splitXiphHeaders(headers, &config)) {
			List<sp<ABuffer> >::iterator it = mConfigs.begin();
			while(it != mConfigs.end()) {
				if((*it)->int32Data() == config->int32Data()) {
					it = mConfigs.erase(it);
					continue;
				}
				++it;
			}
			config->meta()->setInt32("added", 0);
			mConfigs.push_back(config);
			//skip header pages.
			mPageNum += 2;
		} else {
			ALOGI("splitXiphHeaders failed");
			break;
		}
	}
}

sp<ABuffer> AVorbisAssembler::getPackedHeaders(uint32_t ident)
{
	List<sp<ABuffer> >::iterator it = mConfigs.begin();
	while(it != mConfigs.end()) {
		if((*it)->int32Data() == (int32_t)ident) {
			break;
		}
		++it;
	}
	return *it;
}

sp<ABuffer> AVorbisAssembler::generatePageHeader(uint32_t len)
{
	size_t size = 27;
	size += (len / 0x100) + 1;

	CHECK_LE(size - 27, 255);

	sp<ABuffer> header = new ABuffer(size);

	uint8_t *data = header->data();
	memset(data, 0, header->size());

   	data[0] = 0x4f;
	data[1] = 0x67;
	data[2] = 0x67;
	data[3] = 0x53;
	data[5] = mHeaderType;
	data[18] = (mPageNum >>  0) & 0xff;
	data[19] = (mPageNum >>  8) & 0xff;
	data[20] = (mPageNum >> 16) & 0xff;
	data[21] = (mPageNum >> 24) & 0xff;
	mPageNum ++;

	data[26] = size - 27;
	int32_t remainder = len % 0xff;
	CHECK_GE(size, 27);
	memset(&data[27], 0xff, size - 27);

	if(remainder) {
		data[size - 1] = remainder;
	}

	mHeaderType = (data[size - 1] == 0xff) ? 1 : 0;

	return header;
}

void AVorbisAssembler::parseBufferInfo(const sp<ABuffer> &buffer, sp<ABuffer> &accessUnit)
{
	const uint8_t *data = buffer->data();

	uint32_t dataType = (data[3] & 0x30)>>4;

	if(dataType == PackedConfigurationPayload) {
		parsePackedConfiguration(buffer, accessUnit);
	} else if(dataType == RawVorbisPayload){
		parseRawPayload(buffer, accessUnit);
	} else {
		TRESPASS();
	}
}

void AVorbisAssembler::parsePackedConfiguration(const sp<ABuffer> &buffer, sp<ABuffer> &accessUnit)
{
	uint32_t len	= 0;
	size_t off		= 0;
	size_t size 	= 0;

	size = accessUnit->size();
	off += 4;

	const uint8_t *data = buffer->data();

	while(off < buffer->size()) {
		len = u16at(&data[off]);
		off += 2;
		sp<ABuffer> header = generatePageHeader(len);
		memcpy(accessUnit->data() + size, header->data(), header->size());
		size += header->size();
		memcpy(accessUnit->data() + size, &data[off], len);
		size += len;
		off  += len;
	}
	accessUnit->setRange(0, size);
}

void AVorbisAssembler::parseRawPayload(const sp<ABuffer> &buffer, sp<ABuffer> &accessUnit)
{
	uint32_t i 		= 0;

	uint32_t len	= 0;
	size_t off		= 0;
	size_t size 	= 0;

	uint32_t packetsNum = buffer->data()[3] & 0x0f;

	size = accessUnit->size();
	off += 4;

	bool adjustLen = isAdjustLengthNeccessary(buffer);

	const uint8_t *data = buffer->data();
	while(off < buffer->size()) {
		//
		len = u16at(&data[off]);
		if(adjustLen && (len != buffer->size() - 4)) {
			len = buffer->size() - 4;
		} else {
			off += 2;
		}

		sp<ABuffer> header = generatePageHeader(len);
		memcpy(accessUnit->data() + size, header->data(), header->size());
		size += header->size();
		memcpy(accessUnit->data() + size, &data[off], len);
		size += len;
		off  += len;
		i ++;
		if(i >= packetsNum) {
			break;
		}
	}
	ALOGV("i %d, %d, capacity %d, size %d", i, buffer->data()[3] & 0x0f, accessUnit->capacity(), size);
	CHECK(i == packetsNum);
	accessUnit->setRange(0, size);
}

}  // namespace android
