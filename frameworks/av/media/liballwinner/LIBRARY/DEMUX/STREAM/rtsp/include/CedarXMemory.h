#ifndef CEDARX_MEMORY_H
#define CEDARX_MEMORY_H
//#include <stdio.h>
#define CdxMalloc(size) malloc(size)
#define CdxFree(ptr) free(ptr)
#define CdxMemcpy(dest, src, n) memcpy(dest, src, n)
#define CdxMemset(s, c, n) memset(s, c, n)
#define CdxStrdup(s) strdup(s)
#define CdxStrlen(s) strlen(s)
#endif
