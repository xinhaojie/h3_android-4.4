#ifndef CEDARX_STREAM_H
#define CEDARX_STREAM_H

typedef long long int64_t;
enum CdxStreamTypeE {
    CDX_STREAM_HTTP = 0,
    CDX_STREAM_RTSP,
    CDX_STREAM_HLS,
    CDX_STREAM_MMS,
    CDX_STREAM_RTMP,
    CDX_STREAM_UDP,
    CDX_STREAM_M3U8,
    CDX_STREAM_FILE,
    CDX_STREAM_FD,
#ifdef ANDROID
    CDX_STREAM_WIFI_DISPLAY,
    CDX_STREAM_DRM,
#endif
    CDX_STREAM_NUM
};

struct CedarXStreamInfo 
{
    /*
        *文档里面的是: int (*read)(void*, int, CedarXStreamInfo*)
        */
    int (*read)(struct CedarXStreamInfo *, void *, int);
    int64_t (*seek)(struct CedarXStreamInfo *, int64_t, int);
    int64_t (*tell)(struct CedarXStreamInfo *);
    int (*eof)(struct CedarXStreamInfo *);
    int64_t (*seekToTime)(struct CedarXStreamInfo *, int64_t);
    int64_t (*length)(struct CedarXStreamInfo *);
    int (*cacheSize)(struct CedarXStreamInfo *);
    int (*control)(struct CedarXStreamInfo*, int, void*);
    int mIsNetworkStream;
};

/*
  *关于该结构体定义的意见:
  *1.专用的不应该放到这里来，例如HTTP相关的，
  *     可以用保留位来给各类型扩展
  *2.mHttpHeader可否直接用char *? 按照格式解析即可，
  *     二维数组比较考验内存分配
  *3.
  */
struct CedarXDataSource
{
    char *mUrl;
    int *mFd;
    int64_t mFdOffset;
    int64_t mFdLength;
    void *mDataInterface;
    int mDataInterfaceId;
    int mIsBluerayDisk;
    int mHttpHeaderSize;
    char ***mHttpHeader;
    void *mHttpCore;
    struct CedarXStreamInfo *mStreamInfo;
};

struct CedarXStreamEntryS
{
    struct CedarXStreamInfo *(*open)(struct CedarXDataSource *, void *);
    int (*close)(struct CedarXStreamInfo *);
};

#ifdef __cplusplus
extern "C"
{
#endif

static inline int CedarXStreamRead(struct CedarXStreamInfo * s, void *buf, 
                                int len)
{
    return s->read(s, buf, len);
}


static inline int64_t CedarXStreamSeek(struct CedarXStreamInfo *s, 
                                        int64_t offset, int whence)
{
    return s->seek(s, offset, whence);
}

static inline int64_t CedarXStreamTell(struct CedarXStreamInfo *s)
{
    return s->tell(s);
}

static inline int CedarXStreamEof(struct CedarXStreamInfo *s)
{
    return s->eof(s);
}

static inline int64_t CedarXStreamSeekToTime(struct CedarXStreamInfo *s, 
                                            int64_t time)
{
    return s->seekToTime(s, time);
}

static inline int64_t CedarXStreamLength(struct CedarXStreamInfo *s)
{
    return s->length(s);
}

static inline int CedarXStreamCacheSize(struct CedarXStreamInfo *s)
{
    return s->cacheSize(s);
}

static inline int CedarXStreamControl(struct CedarXStreamInfo *s, int cmd, 
                                    void *arg)
{
    return s->control(s, cmd, arg);
}

struct CedarXStreamInfo *CedarXStreamOpen(enum CdxStreamTypeE type, 
                                    struct CedarXDataSource *source, void *flag);

int CedarXStreamClose(enum CdxStreamTypeE type, struct CedarXStreamInfo *s);

int CedarXStreamRegister(enum CdxStreamTypeE type, struct CedarXStreamEntryS *entry);

int CedarXStreamUnregister(enum CdxStreamTypeE type);

#ifdef __cplusplus
}
#endif

#endif
