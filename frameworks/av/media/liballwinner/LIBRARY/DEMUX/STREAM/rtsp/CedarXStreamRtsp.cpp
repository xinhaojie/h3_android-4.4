
#include <CdxTypes.h>
#include <CdxLog.h>
//#include <CedarXTypes.h>
//#include <CedarXLog.h>
#include <CedarXStreamErrno.h>
#include <CedarXRtspSpec.h>
#include <CedarXStream.h>
#include <RTSPSource.h>
#include <ABuffer.h>
#include <AMessage.h>
#include <ADebug.h>
#include <ATSParser.h>
//#include <cedarx_demux.h>
#include <CedarXMemory.h>
#include <Utils.h>
#include <ESDS.h>
//#include <epdk_abs_header.h>

#include <CdxStream.h>
extern "C" 
{
#include <adecoder.h>
#include <vdecoder.h>
#include <tmessage.h>
}

/*must be cut off*/
#include <media/stagefright/MediaDefs.h>
#include <media/stagefright/MetaData.h>
#include <media/stagefright/MediaErrors.h>

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "RtspStream"

#define SCAN_TIMEOUT_US 					(200 * 1000 * 1000) //200s
#define SCAN_TIMEOUT_AFTER_FIRST_STREAM_US 	(4 * 1000 * 1000) //4s
#define SCAN_UNIT_US    					(20 * 1000)
#define MAX_CACHED_PACKET 256

enum SOURCETYPE {
	VIDEO_TYPE = 0,
	AUDIO_TYPE    ,
};

using namespace android;

enum CedarXStreamWhence
{
    CDX_STREAM_META_HEADER = 0,
    CDX_STREAM_META_VIDEO,
    CDX_STREAM_META_VIDEO_EXTRA,
    CDX_STREAM_META_AUDIO,
    CDX_STREAM_META_AUDIO_EXTRA,
    CDX_STREAM_CONTENT_PKT_HEAD,
    CDX_STREAM_CONTENT_PKT_DATA,
};
struct CedarXRtspPosS
{
	enum CedarXStreamWhence whence;
    cdx_uint32 offset;
};

struct demux_rtsp_context
{
	pthread_t	thread_id;
	cdx_int32	thread_exit;
	cdx_int32   pause_fetch;
	cdx_int32   fetch_is_puase;
	cdx_uint32	has_audio;
	cdx_uint32	has_video;
	cdx_int64	fetch_packet_time;
	cdx_int64	read_packet_time;
};

struct CedarXStreamRtspImplS
{
    CdxStreamT base;
	sp<Source> mSource;
    struct CedarXRtspPosS mPos;
	cdx_int64 mBaseTime[2];
	cdx_int64 mRecentTime[2];
	struct demux_rtsp_context mCtx;
	struct CedarXRtspMetaS mMeta;
	struct message_queue_t mMsgQueue;
	struct CedarXRtspPktS *mCurPtk;
	int ioState;
	CdxStreamProbeDataT probeData;
    cdx_char *url;
};
struct CedarXRtspPktS
{
    struct CedarXRtspPktHeadS pktHeader;
    sp<ABuffer> unit;
};

#ifdef __cplusplus
extern "C"
{
#endif

static int CedarXRtspReadPacket(struct CedarXStreamRtspImplS *impl,
                            cdx_bool audio, struct CedarXRtspPktS *pkt)
{
    sp<ABuffer> accessUnit;
    int64_t mediaTimeUs;
    status_t err;
    status_t ret;

    err = impl->mSource->dequeueAccessUnit(audio, &accessUnit);
    if (err == -EWOULDBLOCK) {
		if ((ret = impl->mSource->feedMoreTSData()) != CDX_SUCCESS) {
			CDX_LOGW("feedMoreTSData fail");
			return ret;
		}
        return err;
    }
    else if (err != CDX_SUCCESS) {
        if (err == INFO_DISCONTINUITY) {
        	int32_t type;
        	CHECK(accessUnit->meta()->findInt32("discontinuity", &type));

        	bool formatChange =
				(audio && (type & ATSParser::DISCONTINUITY_AUDIO_FORMAT))
				|| (!audio && (type & ATSParser::DISCONTINUITY_VIDEO_FORMAT));
			bool timeChange = (type & ATSParser::DISCONTINUITY_TIME) != 0;
			CDX_LOGI("%s discontinuity (formatChange=%d, time=%d)",
				 audio ? "audio" : "video", formatChange, timeChange);

			if (timeChange) {
				sp<AMessage> extra;
				if (accessUnit->meta()->findMessage("extra", &extra)
						&& extra != NULL) {
					int64_t resumeAtMediaTimeUs;
					if (extra->findInt64("resume-at-mediatimeUs", 
                                                    &resumeAtMediaTimeUs)) 
					{
						CDX_LOGI("suppressing rendering of %s until %lld us",
								audio ? "audio" : "video", resumeAtMediaTimeUs);

					}
				}

				impl->mBaseTime[audio] = impl->mRecentTime[audio];
				accessUnit->meta()->findInt64("timeUs", &mediaTimeUs);
				if (mediaTimeUs > 40*1000) {
					impl->mBaseTime[audio] -= mediaTimeUs;
				}
			}
        }
        else if(err == INFO_FORMAT_CHANGED) {
        	return CDX_SUCCESS;
        }
        else {
        	return err;
        }
    }
    accessUnit->meta()->findInt64("timeUs", &mediaTimeUs); 
    mediaTimeUs += impl->mBaseTime[audio];
    impl->mRecentTime[audio] = mediaTimeUs;

	pkt->pktHeader.type = audio ? CDX_MEDIA_AUDIO : CDX_MEDIA_VIDEO;
	pkt->pktHeader.lenght = accessUnit->size();
	pkt->pktHeader.pts = mediaTimeUs;
	pkt->unit = accessUnit.get(); //  // ref += 1 ,ref  = 2
//	accessUnit->incStrong(NULL); // ref += 1
    return CDX_SUCCESS;
    // function exit, accessUnit destrouct  ref -= 1, ref = 1
}

static void *CedarXRtspDataProcess(void* pThreadData)
{
    struct CedarXStreamRtspImplS *impl;
	demux_rtsp_context *dmx_rtsp_ctx;
	cdx_int32 ret = 0;
	cdx_int64 ad_pts = -1, vd_pts = -1;
    cdx_bool isAudio = CDX_FALSE;
	message_t msg;
	struct CedarXRtspPktS *pkt = NULL;

    impl = (struct CedarXStreamRtspImplS *)pThreadData;
    dmx_rtsp_ctx = &impl->mCtx;
    
	while(!dmx_rtsp_ctx->thread_exit)
	{
		if(get_message_count(&impl->mMsgQueue) > MAX_CACHED_PACKET 
		        || dmx_rtsp_ctx->pause_fetch)
		{
			dmx_rtsp_ctx->fetch_is_puase = 1;
			CDX_LOGD("cache is full debug t0:%lld t1:%lld",
			    dmx_rtsp_ctx->fetch_packet_time, dmx_rtsp_ctx->read_packet_time);
			usleep(20*1000);
			continue;
		}

		dmx_rtsp_ctx->fetch_is_puase = 0;

		if((dmx_rtsp_ctx->has_video && vd_pts <= ad_pts) 
		        || !dmx_rtsp_ctx->has_audio)
		{
			isAudio = CDX_FALSE;
		}
		else if(dmx_rtsp_ctx->has_audio)
		{
			isAudio = CDX_TRUE;
		}
		
		pkt = (struct CedarXRtspPktS *)CdxMalloc(sizeof(struct CedarXRtspPktS));
        if(pkt == NULL)
		{
			CDX_LOGE("no mem");
			goto EXIT;
		}
        CdxMemset(pkt, 0x00, sizeof(struct CedarXRtspPktS));
		
refetch:
		ret = CedarXRtspReadPacket(impl, isAudio, pkt);
		if(ret == -EWOULDBLOCK)
		{
			CDX_LOGV("read would block!");
			usleep(50*1000);
			if(!dmx_rtsp_ctx->thread_exit){
				goto refetch;
			}
		}
		else if (ret != 0)
		{
			CDX_LOGI("stream ended?");
			dmx_rtsp_ctx->thread_exit = 1;
			break;
		}
		
		if(!isAudio) /* video */
		{
			vd_pts = pkt->pktHeader.pts;
			dmx_rtsp_ctx->fetch_packet_time = pkt->pktHeader.pts;
			if(dmx_rtsp_ctx->read_packet_time == -1)
			{
				dmx_rtsp_ctx->read_packet_time = pkt->pktHeader.pts;
			}
		}
		else    /* audio */
		{
			ad_pts = pkt->pktHeader.pts;
			if(!dmx_rtsp_ctx->has_video)
			{
				dmx_rtsp_ctx->fetch_packet_time = pkt->pktHeader.pts;
				if(dmx_rtsp_ctx->read_packet_time == -1)
				{
					dmx_rtsp_ctx->read_packet_time = pkt->pktHeader.pts;
				}
			}
		}
		
		msg.data = pkt;
		put_message(&impl->mMsgQueue, &msg);
	}

EXIT:
	dmx_rtsp_ctx->thread_exit = 1;
	return NULL;
}

static int CedarXRtspMetaParser(sp<MetaData> meta, struct CedarXRtspMetaS *rtspMeta)
{
	const char *mime;
	int64_t totalBitRate = 0;
	int32_t width, height, channels, sample_rate, extra_data;
	uint32_t type;
	const void *data;
	size_t size;

	meta->findCString(kKeyMIMEType, &mime);
	CDX_LOGV("kKeyMIMEType: %s", mime);

	if (!strncasecmp(mime, "video/", 6))
	{
		if (!strcasecmp(mime, MEDIA_MIMETYPE_VIDEO_VPX))
		{
		    rtspMeta->videoRegion.codingType = VIDEO_CODEC_FORMAT_VP8;
		}
		else if (!strcasecmp(mime, MEDIA_MIMETYPE_VIDEO_AVC))
		{
			rtspMeta->videoRegion.codingType = VIDEO_CODEC_FORMAT_H264;
//			rtspMeta->videoRegion.codingSubType = 0;
		} 
		else if (!strcasecmp(mime, MEDIA_MIMETYPE_VIDEO_MPEG4))
		{
			rtspMeta->videoRegion.codingType = VIDEO_CODEC_FORMAT_XVID;
//			rtspMeta->videoRegion.codingSubType = CEDARV_MPEG4_SUB_FORMAT_XVID;
		}
		else if (!strcasecmp(mime, MEDIA_MIMETYPE_VIDEO_H263))
		{
			rtspMeta->videoRegion.codingType = VIDEO_CODEC_FORMAT_H263;
//			rtspMeta->videoRegion.codingSubType = CEDARV_MPEG4_SUB_FORMAT_H263;
		}
		else if(!strcasecmp(mime, MEDIA_MIMETYPE_VIDEO_MPEG2))
		{
			rtspMeta->videoRegion.codingType = VIDEO_CODEC_FORMAT_MPEG2;
		}

		if (meta->findData(kKeyAVCC, &type, &data, &size)) {
			int size0,size1;
			const uint8_t *ptr = (const uint8_t *)data;

			CHECK(size >= 7);
			CHECK_EQ((unsigned)ptr[0], 1u);  // configurationVersion == 1
			uint8_t profile = ptr[1];
			uint8_t level = ptr[3];
			size_t lengthSize = 1 + (ptr[4] & 3);
			size_t numSeqParameterSets = ptr[5] & 31;

			ptr += 6;
			size -= 6;

			sp<ABuffer> buffer = new ABuffer(1024);
			buffer->setRange(0, 0);

			for (size_t i = 0; i < numSeqParameterSets; ++i) {
				CHECK(size >= 2);
				size_t length = U16_AT(ptr);

				ptr += 2;
				size -= 2;

				CHECK(size >= length);

				memcpy(buffer->data() + buffer->size(), "\x00\x00\x00\x01", 4);
				memcpy(buffer->data() + buffer->size() + 4, ptr, length);
				buffer->setRange(0, buffer->size() + 4 + length);

				ptr += length;
				size -= length;
			}

			CHECK(buffer->size() < 1024);

			rtspMeta->videoExtraData = (char *)CdxMalloc(2048);
			size0 = buffer->size();
			memcpy(rtspMeta->videoExtraData, buffer->data(), buffer->size());

			buffer->setRange(0,0);
			CHECK(size >= 1);
			size_t numPictureParameterSets = *ptr;
			++ptr;
			--size;

			for (size_t i = 0; i < numPictureParameterSets; ++i) {
				CHECK(size >= 2);
				size_t length = U16_AT(ptr);

				ptr += 2;
				size -= 2;

				CHECK(size >= length);

				memcpy(buffer->data() + buffer->size(), "\x00\x00\x00\x01", 4);
				memcpy(buffer->data() + buffer->size() + 4, ptr, length);
				buffer->setRange(0, buffer->size() + 4 + length);

				ptr += length;
				size -= length;
			}

			size1 = buffer->size();
			CHECK(size0+size1 < 2048);
			memcpy(rtspMeta->videoExtraData + size0, buffer->data(), buffer->size());
            rtspMeta->videoRegion.extraDataLen = size0 + size1;

		}
		else if (meta->findData(kKeyESDS, &type, &data, &size)) 
		{
	    	ESDS esds((const char *) data, size);
			esds.InitCheck();

			const void *codec_specific_data;
			size_t codec_specific_data_size;
			esds.getCodecSpecificInfo(&codec_specific_data,
					&codec_specific_data_size);

	    	rtspMeta->videoRegion.extraDataLen = codec_specific_data_size;
			rtspMeta->videoExtraData = (char *)CdxMalloc(rtspMeta->videoRegion.extraDataLen);
			memcpy(rtspMeta->videoExtraData, codec_specific_data, 
			        rtspMeta->videoRegion.extraDataLen);
	    }
	    else
	    {
			rtspMeta->videoRegion.extraDataLen = 0;
		}

		meta->findInt32(kKeyWidth, &width);
		meta->findInt32(kKeyHeight, &height);
		rtspMeta->videoRegion.width = width;
		rtspMeta->videoRegion.height = height;
		rtspMeta->videoRegion.ptsCorrect  = 1;
		rtspMeta->videoRegion.thirdParaOverride = 1;
        rtspMeta->header.vidStrmNum++;
		rtspMeta->header.hasVideo++;
	} 
	else if (!strncasecmp(mime, "audio/", 6)) 
	{
		if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_AMR_NB)) 
		{
		    rtspMeta->audioRegion.codingType = AUDIO_CODEC_FORMAT_AMR;
			rtspMeta->audioRegion.subCodingType = AMR_FORMAT_NARROWBAND;
		} 
		else if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_AMR_WB)) 
		{
			rtspMeta->audioRegion.codingType = AUDIO_CODEC_FORMAT_AMR;
			rtspMeta->audioRegion.subCodingType = AMR_FORMAT_WIDEBAND;
		} 
		else if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_AAC))
		{
			rtspMeta->audioRegion.codingType = AUDIO_CODEC_FORMAT_MPEG_AAC_LC;
		}
		else if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_MPEG_LAYER_II)) 
		{
			rtspMeta->audioRegion.codingType = AUDIO_CODEC_FORMAT_MP2;
		}
		else if (!strcasecmp(mime, MEDIA_MIMETYPE_AUDIO_VORBIS))
		{
			rtspMeta->audioRegion.codingType = AUDIO_CODEC_FORMAT_OGG;
		}

		meta->findInt32(kKeyChannelCount, &channels);
		meta->findInt32(kKeySampleRate, &sample_rate);

		rtspMeta->audioRegion.channels = channels;
		rtspMeta->audioRegion.samplePerSecond = sample_rate;

		if (meta->findData(kKeyESDS, &type, &data, &size))
        {
            char *tmpStr = (char *)data;
			ESDS esds((const char *) data, size);
			esds.InitCheck();

			const void *codec_specific_data;
			size_t codec_specific_data_size;
			esds.getCodecSpecificInfo(&codec_specific_data,
					&codec_specific_data_size);
			rtspMeta->audioExtraData = (char *)CdxMalloc(codec_specific_data_size);
			if (rtspMeta->audioExtraData == NULL) 
            {
				CDX_LOGW("malloc error!");
				return -1;
			}
			memcpy(rtspMeta->audioExtraData, codec_specific_data,
					codec_specific_data_size);
			rtspMeta->audioRegion.extraDataLen = codec_specific_data_size;
		}
		else
		{
			rtspMeta->audioRegion.extraDataLen = 0;
		}

		rtspMeta->header.audStrmNum++;
		rtspMeta->header.hasAudio++;
	}

	return CDX_SUCCESS;
}
/*
  *  +--------+-----------+---------+------------+----------+
  *  +  header   + video region + extra data +  audio region  + extra data   +
  *  +--------+-----------+---------+------------+----------+
  */
static int __CdxRtspStreamRead(CdxStreamT * s, void *buf, cdx_uint32 len)
{
    struct CedarXStreamRtspImplS *impl;
    int read = 0;
    int unitLen = 0;
    
    impl = CdxContainerOf(s, struct CedarXStreamRtspImplS, base);

readProc:
    switch (impl->mPos.whence)
    {
        case CDX_STREAM_META_HEADER:
        {
            unitLen = CedarXMin(len - read, 
                            sizeof(struct CedarXRtspHeaderS) - impl->mPos.offset);
            memcpy((char *)buf + read, 
                       ((char *)&impl->mMeta.header) + impl->mPos.offset, unitLen);
            impl->mPos.offset += unitLen;
            read += unitLen;
            if (impl->mPos.offset == sizeof(struct CedarXRtspHeaderS))
            {   /*read all header region data*/
                impl->mPos.offset = 0;
                impl->mPos.whence = CDX_STREAM_META_VIDEO;
            }
            break;
        }
        case CDX_STREAM_META_VIDEO: 
        {
            if (!impl->mMeta.header.hasVideo) /*if no video*/
            {
                impl->mPos.whence = CDX_STREAM_META_AUDIO;
                goto readProc;
            }
            unitLen = CedarXMin(len - read, 
                        sizeof(struct CedarXRtspVideoRegionS) - impl->mPos.offset);
            memcpy((char *)buf + read, 
                       ((char *)&impl->mMeta.videoRegion) + impl->mPos.offset, unitLen);
            impl->mPos.offset += unitLen;
            read += unitLen;
            if (impl->mPos.offset == sizeof(struct CedarXRtspVideoRegionS))
            {   /*read all video region data*/
                impl->mPos.offset = 0;
                impl->mPos.whence = CDX_STREAM_META_VIDEO_EXTRA;
            }
            break;
        }
        case CDX_STREAM_META_VIDEO_EXTRA: 
        {
            if (!impl->mMeta.videoRegion.extraDataLen)
            {
                impl->mPos.whence = CDX_STREAM_META_AUDIO;
                goto readProc;
            }
            unitLen = CedarXMin(len - read, 
                        impl->mMeta.videoRegion.extraDataLen - impl->mPos.offset);
            memcpy((char *)buf + read, 
                       impl->mMeta.videoExtraData + impl->mPos.offset, unitLen);
            impl->mPos.offset += unitLen;
            read += unitLen;
            if (impl->mPos.offset == impl->mMeta.videoRegion.extraDataLen)
            {
                impl->mPos.offset = 0;
                impl->mPos.whence = CDX_STREAM_META_AUDIO;
            }
            break;
        }
            
        case CDX_STREAM_META_AUDIO:
        {
            if (!impl->mMeta.header.hasAudio) 
            {
                impl->mPos.whence = CDX_STREAM_CONTENT_PKT_HEAD;
                goto readProc;
            }
            unitLen = CedarXMin(len - read, 
                        sizeof(struct CedarXRtspAudioRegionS) - impl->mPos.offset);
            memcpy((char *)buf + read, 
                  ((char *)&impl->mMeta.audioRegion) + impl->mPos.offset, unitLen);
            impl->mPos.offset += unitLen;
            read += unitLen;
            if (impl->mPos.offset == sizeof(struct CedarXRtspAudioRegionS))
            {
                impl->mPos.offset = 0;
                impl->mPos.whence = CDX_STREAM_META_AUDIO_EXTRA;
            }
            break;
        }
        
        case CDX_STREAM_META_AUDIO_EXTRA:
        {
            if (!impl->mMeta.audioRegion.extraDataLen)
            {
                impl->mPos.whence = CDX_STREAM_CONTENT_PKT_HEAD;
                goto readProc;
            }
            unitLen = CedarXMin(len - read, 
                        impl->mMeta.audioRegion.extraDataLen - impl->mPos.offset);
            memcpy((char *)buf + read, 
                       impl->mMeta.audioExtraData + impl->mPos.offset, unitLen);
            impl->mPos.offset += unitLen;
            read += unitLen;
            if (impl->mPos.offset == impl->mMeta.audioRegion.extraDataLen)
            {
                impl->mPos.offset = 0;
                impl->mPos.whence = CDX_STREAM_CONTENT_PKT_HEAD;
            }
            break;
        }
        
        case CDX_STREAM_CONTENT_PKT_HEAD:
        {
            if (!impl->mCurPtk) /*need to get a packet from msg queue*/
            {
                message_t msg;
                if(get_message(&impl->mMsgQueue, &msg) == 0)
                {
                    impl->mCurPtk = (struct CedarXRtspPktS *)msg.data;
                    impl->mCtx.read_packet_time = impl->mCurPtk->pktHeader.pts;
                }
                else /*no msg in queue*/
                {
    //                CDX_LOGW("rtsp prefetch empty");
                    usleep(50000);
                    goto readProc;
                }
            }
            unitLen = CedarXMin(len - read, CEDARX_PKT_HEAD_SIZE - impl->mPos.offset);
            memcpy((char *)buf + read, 
                       ((char *)impl->mCurPtk) + impl->mPos.offset, unitLen);
            impl->mPos.offset += unitLen;
            read += unitLen;
            if (impl->mPos.offset == CEDARX_PKT_HEAD_SIZE)
            {
                impl->mPos.offset = 0;
                impl->mPos.whence = CDX_STREAM_CONTENT_PKT_DATA;
            }
            break;
        }
        case CDX_STREAM_CONTENT_PKT_DATA:
        {
            unitLen = CedarXMin(len - read, 
                               impl->mCurPtk->pktHeader.lenght - impl->mPos.offset);
            memcpy((char *)buf + read, 
               ((char *)impl->mCurPtk->unit->data()) + impl->mPos.offset, unitLen);
            impl->mPos.offset += unitLen;
            read += unitLen;
            if (impl->mPos.offset == impl->mCurPtk->pktHeader.lenght)
            {
                impl->mCurPtk->unit->decStrong(NULL);
                CdxFree(impl->mCurPtk);
                impl->mCurPtk = NULL;
                impl->mPos.offset = 0;
                impl->mPos.whence = CDX_STREAM_CONTENT_PKT_HEAD;
            }
            break;
        }
        default:
            CDX_LOGE("unexpect case!!!");
            break;
    }
    if (read < (int)len)
    {
        goto readProc;
    }
    
readTerMinate:
    return read;    
}

/*
    cdx_int32 (*seek)(CdxStreamT * , cdx_int64 , cdx_int32 );
*/
static cdx_int32 __CdxRtspStreamSeek(CdxStreamT *s, int64_t offset,
                            int whence)
{
	CDX_UNUSE(s);
	CDX_UNUSE(offset);
	CDX_UNUSE(whence);
    return 0;
}

//    cdx_int64 (*tell)(CdxStreamT * );
static int64_t CedarXStreamRtspTell(CdxStreamT *s)
{
	CDX_UNUSE(s);
    return 0;
}

//    cdx_bool (*eos)(CdxStreamT * );
static int CedarXStreamRtspEof(CdxStreamT *s)
{
	CDX_UNUSE(s);
    return 0;
}


static int64_t CedarXStreamRtspLength(CdxStreamT *s)
{
	CDX_UNUSE(s);
    return 0;
}

// cdx_int64 (*size)(CdxStreamT * );

static int CedarXStreamRtspCacheSize(CdxStreamT *s)
{
	CDX_UNUSE(s);
    return 0;
}

static cdx_int32 __CdxRtspStreamControl(CdxStreamT *s, int cmd, 
                                        void *arg)
{
    struct CedarXStreamRtspImplS *impl;
    
    impl = CdxContainerOf(s, struct CedarXStreamRtspImplS, base);
    
	switch(cmd)
	{
	case STREAM_CMD_GET_CACHESTATE: 
	{
	    struct StreamCacheStateS *cacheState = (struct StreamCacheStateS *)arg;
        cacheState->nCacheCapacity = 1>>20; // 1MB
        cacheState->nCacheSize = 0; // 1MB
        cacheState->nBandwidthKbps = -1;
        cacheState->nPercentage = 0;
        
        if(impl->mCtx.read_packet_time != -1)
        {   
            cacheState->nPercentage = get_message_count(&impl->mMsgQueue)*100 / MAX_CACHED_PACKET;
            CDX_LOGV("fetch_packet_time:%lld read_packet_time:%lld diff:%lld "
                "filled_percent: %d", impl->mCtx.fetch_packet_time/1000,
                impl->mCtx.read_packet_time/1000,
                impl->mCtx.fetch_packet_time/1000 - impl->mCtx.read_packet_time/1000,
                cacheState->nPercentage);
        }
        else
        {
            cacheState->nPercentage = 0;
        }
		break;
    }
    default:
        break;
    }
    return CDX_SUCCESS;
}

static CdxStreamProbeDataT *__CdxRtspStreamGetProbeData(CdxStreamT *s)
{
    struct CedarXStreamRtspImplS *impl;
    impl = CdxContainerOf(s, struct CedarXStreamRtspImplS, base);

    return &impl->probeData;
}

static cdx_int32 __CdxRtspStreamGetIOState(CdxStreamT *s)
{
    struct CedarXStreamRtspImplS *impl;    
    impl = CdxContainerOf(s, struct CedarXStreamRtspImplS, base);
    
    return impl->ioState;
}

static cdx_uint32 __CdxRtspStreamAttribute(CdxStreamT *s)
{
    struct CedarXStreamRtspImplS *impl;    
    impl = CdxContainerOf(s, struct CedarXStreamRtspImplS, base);

    return /*CDX_STREAM_FLAG_STT|*/CDX_STREAM_FLAG_NET;
}

static cdx_int32 __CdxRtspStreamForceStop(CdxStreamT *s)
{
    struct CedarXStreamRtspImplS *impl;
    impl = CdxContainerOf(s, struct CedarXStreamRtspImplS, base);

    CDX_LOGW("not implement now...");
    return -1;
}

static void CedarXStreamRtspObjDestroy(struct CedarXStreamRtspImplS * obj)
{
    CdxFree(obj);
    return ;
}

static int __CdxRtspStreamClose(CdxStreamT *s)
{
    struct CedarXStreamRtspImplS *impl;
    
    if (!s)
    {
        CDX_LOGE("stream info is null.");
        return CEDARX_STREAM_ERR_PARAMETER_INVAILD;
    }
    impl = CdxContainerOf(s, struct CedarXStreamRtspImplS, base);

	impl->mCtx.thread_exit = 1;
	pthread_join(impl->mCtx.thread_id, NULL);
    free(impl->url);
    if(impl->mSource != NULL)
    {
        impl->mSource->stop();
        impl->mSource.clear();
    }
    //destroy other data, extradata,msgQueue etc...
    CedarXStreamRtspObjDestroy(impl);
    return CDX_SUCCESS;
}

static cdx_int32 __CdxRtspStreamConnect(CdxStreamT *stream)
{
    struct CedarXStreamRtspImplS *impl;
    cdx_int32 result;
    int i = 0;
    int retryTime = 0;
	const char *mime;
    status_t err;
	sp<MetaData> meta;
	int scan_audio = 0;
	int media_found = 0;
	int64_t durationUs = 0;

    CDX_CHECK(stream);
    impl = CdxContainerOf(stream, struct CedarXStreamRtspImplS, base);

    impl->mSource = new RTSPSource(impl->url, NULL, true, 222); 
    impl->mSource->start();

    retryTime = SCAN_TIMEOUT_US/SCAN_UNIT_US;
	
	for(i = 0; i < retryTime; i++)
	{
		if ((err = impl->mSource->feedMoreTSData()) != OK) 
		{
			CDX_LOGW("TODO: howto process it? err=%d",err);
			break;
		}

		meta = impl->mSource->getFormat(scan_audio);

		if (meta == NULL) 
		{
			usleep(SCAN_UNIT_US);
		}
		else
		{
			int64_t audioDurationUs;
			meta->findCString(kKeyMIMEType, &mime);
#if 0
			if(!strncasecmp("video/x-asf-pf", mime, 14)
					|| !strncasecmp("audio/x-asf-pf", mime, 14)) 
			{
				if(!meta->findInt64(kKeyDuration, &audioDurationUs) 
				        || !audioDurationUs)
				{
					impl->mMeta.header.disableSeek = 1;
				}
				
				impl->mMeta.header.demuxType = CDX_MEDIA_FILE_FMT_ASF;
				impl->mMeta.header.sftRtspSource = impl->mSource.get();
				goto successful;
			}
			else if(!strncasecmp("video/mp2t", mime, 10))
			{			
				if(!meta->findInt64(kKeyDuration, &audioDurationUs) 
				        || !audioDurationUs)
				{
					impl->mMeta.header.disableSeek = 1;
				}
				impl->mMeta.header.demuxType = CDX_MEDIA_FILE_FMT_TS;
				impl->mMeta.header.sftRtspSource = impl->mSource.get();
				goto successful;
			}
#endif
			CDX_LOGV("media fount av: %d", scan_audio);
			media_found |= 1 << scan_audio;
			if (media_found == 3)
			{
				break;
			}
			else
			{
				scan_audio = !scan_audio;
				//we must speed up scan as for some stream only has video
				i = (SCAN_TIMEOUT_US - SCAN_TIMEOUT_AFTER_FIRST_STREAM_US)
				        / SCAN_UNIT_US;
			}
		}

		if (!media_found) 
        {
			scan_audio = !scan_audio;
		}
	}
	CDX_LOGD("media found result: %d", media_found);
	
	if (!media_found)
	{
		goto failure;
    }
    
	for(i = 0; i < 2; i++) 
    {
		meta = impl->mSource->getFormat(i);
		if(meta != NULL) 
        {
			CedarXRtspMetaParser(meta, &impl->mMeta);
		}
	}

    if (impl->mSource->getDuration(&durationUs) == OK) 
    {
    	impl->mMeta.header.duration = durationUs / 1000;
    }
    impl->mMeta.header.streamNum = impl->mMeta.header.audStrmNum 
                                + impl->mMeta.header.vidStrmNum;

	impl->mCtx.has_audio = impl->mMeta.header.audStrmNum;
	impl->mCtx.has_video = impl->mMeta.header.vidStrmNum;
	impl->mCtx.read_packet_time = -1;
	
	if(!impl->mCtx.has_audio && !impl->mCtx.has_video)
	{
		CDX_LOGE("no av found!");
		goto failure;
	}
	
	if(message_create(&impl->mMsgQueue)<0)
	{
		CDX_LOGE("message error!");
		goto failure;
	}
	
	err = pthread_create(&impl->mCtx.thread_id, NULL, CedarXRtspDataProcess, impl);
	if (err || !impl->mCtx.thread_id)
	{
		CDX_LOGE("create thread error!");
		goto failure; /*should destroy msgQueue*/
	}

successful:
    memcpy(impl->mMeta.header.tag, "remurtsp", 8);
	impl->probeData.buf = &impl->mMeta.header.tag[0];
	impl->probeData.len = 8;
    impl->mPos.whence = CDX_STREAM_META_HEADER;
    impl->mPos.offset = 0;
    return 0;
    
failure:
    impl->mSource->stop();
    //impl->mSource->decStrong(NULL);
    impl->mSource.clear();
    impl->mSource = NULL;
    
	return -1;
}

static struct CdxStreamOpsS rtspStreamOps =
{
    .connect        = __CdxRtspStreamConnect,
    .getProbeData   = __CdxRtspStreamGetProbeData,
    .read           = __CdxRtspStreamRead,
    .close          = __CdxRtspStreamClose,
    .getIOState     = __CdxRtspStreamGetIOState,
    .attribute      = __CdxRtspStreamAttribute,
    .control        = __CdxRtspStreamControl,
    .write          = NULL, 
    .getMetaData    = NULL,
    .seek           = __CdxRtspStreamSeek,
    .seekToTime     = NULL,
    .eos            = NULL,
    .tell           = NULL,
    .size           = NULL,
};

static struct CedarXStreamRtspImplS *CedarXStreamRtspObjCreate(void)
{
    struct CedarXStreamRtspImplS *obj = NULL;
    obj = (struct CedarXStreamRtspImplS *)CdxMalloc(sizeof(struct CedarXStreamRtspImplS));
    if (!obj)
    {
        CDX_LOGE("alloc fail, size: %u", sizeof(struct CedarXStreamRtspImplS));
        return NULL;
    }
    CdxMemset(obj, 0x00, sizeof(struct CedarXStreamRtspImplS));
    obj->base.ops = &rtspStreamOps;
    
    return obj;
}

static CdxStreamT *__CdxStreamRtspCreate(CdxDataSourceT *source)
{
    struct CedarXStreamRtspImplS *impl;
    
	if(strncasecmp("rtsp://", source->uri, 7) != 0) {
        CDX_LOGW("not rtsp source");
        return NULL;
    }
    impl = CedarXStreamRtspObjCreate();
    if (NULL == impl)
    {
        CDX_LOGE("CedarXStreamRtspObjCreate fail...");
        return NULL;
    }

    impl->url = CdxStrdup(source->uri);

    return &impl->base;
}

CdxStreamCreatorT rtspStreamCtor =
{
    .create = __CdxStreamRtspCreate,
};


#ifdef __cplusplus
}
#endif

