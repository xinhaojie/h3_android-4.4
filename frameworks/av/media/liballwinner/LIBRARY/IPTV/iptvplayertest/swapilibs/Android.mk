LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

# <modulename>:<filename>
LOCAL_PREBUILT_LIBS := \
	libenca:libenca.a \
	libiconv:libiconv.a \
	libswbase:libswbase.a \
	libswcommon:libswcommon.a \
	libswgraphics_skia:libswgraphics_skia.a \
	libswos:libswos.a \
	libswplayer:libswplayer.a \
	libswplayertest:libswplayertest.a \
	libswstreamqueue:libswstreamqueue.a \
	libswsubtitlerender_skia:libswsubtitlerender_skia.a \
	libswutil:libswutil.a

LOCAL_MODULE_TAGS := optional
include $(BUILD_MULTI_PREBUILT)
