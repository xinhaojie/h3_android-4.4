LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES += main.c

LOCAL_STATIC_LIBRARIES := \
	libswplayertest \
	libswplayer \
	libswstreamqueue \
	libswbase \
	libswcommon \
	libswgraphics_skia \
	libswsubtitlerender_skia \
	libenca \
	libiconv \
	libswos \
	libswutil \
	libCTC_MediaProcessorWrapper \
	libSurface

LOCAL_SHARED_LIBRARIES += \
	libCTC_MediaProcessor \

LOCAL_SHARED_LIBRARIES += \
	libavformat \
	libavcodec \
	libavutil \
	libz

LOCAL_SHARED_LIBRARIES += \
	libcutils \
	libbinder \
	libui \
	libstlport \
	libskia \
	libgui \
	libdl \
	libmedia \
	libutils \
	libui \
	liblog \
	libcutils \
	libbinder

LOCAL_MODULE := iptvplayertest
LOCAL_MODULE_TAGS := optional 

include $(BUILD_EXECUTABLE)
