#ifndef __SWPLAYERDEF_H__
#define __SWPLAYERDEF_H__

#ifdef __cplusplus
extern "C" {
#endif


//事件回调函数接口
typedef int (*player_event_callback)(void *handle, int event_type, uint32_t wparam, uint32_t lparam, uint32_t lextend);


//定义多媒体事件
#define MEDIA_EVENT_NULL            0  //没有事件
#define MEDIA_EVENT_PLAY            1  //正常播放事件
#define MEDIA_EVENT_PAUSE           2  //暂停事件
#define MEDIA_EVENT_TRICKPLAY       3  //trick play事件
#define MEDIA_EVENT_STOP            4
#define MEDIA_EVENT_ERROR           5  //播放出错事件
#define MEDIA_EVENT_PLAYBEGIN       6  //开始播放事件
#define MEDIA_EVENT_PLAYEND         7  //播放结束事件
#define MEDIA_EVENT_STREAM2BEGIN    8  //流回到开始位置
#define MEDIA_EVENT_STREAM2END      9  //流结束事件
#define MEDIA_EVENT_PLAY2BEGIN      10 //播放到开始
#define MEDIA_EVENT_PLAY2END        11 //播放到结束
#define MEDIA_EVENT_BUFFERING       12 //播放缓冲事件
#define MEDIA_EVENT_FMTREFRESH      13 //更新流格式事件
#define MEDIA_EVENT_FMTCHANGED      14 //流格式发生改变
#define MEDIA_EVENT_RECVTIMEOUT     15 //接收数据超时
#define MEDIA_EVENT_RECVNEWDATA     16 //接收数据超时之后, 重新收到数据
#define MEDIA_EVENT_INVALIDCHAIN	18  //防盗链信息检验失败

#define MEDIA_EVENT_START			19
#define MEDIA_EVENT_SEND_RRECHANDLE 20 //pvr平滑播放将rrec句柄传给avstage
#define MEDIA_EVENT_TIMESHIFT_NEW_TIMEDUR 21 //FLAST空间不够，修改时移窗口

#define MEDIA_EVENT_ADEC_DOLBY_DUAL_MONO 22 //dolby dual mono

#define MEDIA_EVENT_FIRSTPTS		24 //解出第一帧

#define MEDIA_EVENT_NETWORKREMEMBE 32	//组播失败记忆
#define MEDIA_EVENT_ADSBEGIN        50 //开始播放广告
#define MEDIA_EVENT_ADSEND          51 //播放广告结束

#define MEDIA_EVENT_CA_KEYCHANGED   60
#define MEDIA_EVENT_CA_VIDEOHIDE 	61
#define MEDIA_EVENT_CA_VIDEORESUME	62

//misc
#define MEDIA_EVENT_TIMEOUT2UNICAST 100 //华为国内标准组播5s无流转单播 
#define MEDIA_EVENT_PIGMODE_CHANGED 101 //pigmode改变时抛此事件, wparam为swpigmode_e


#define MEDIA_EVENT_RECBASE         150
#define MEDIA_EVENT_RECBEGIN        (MEDIA_EVENT_RECBASE + 1) //录制正常启动
#define MEDIA_EVENT_RECSTOP         (MEDIA_EVENT_RECBASE + 2) //录制正常启动
#define MEDIA_EVENT_RECERROR        (MEDIA_EVENT_RECBASE + 3) //录制正常启动
#define MEDIA_EVENT_RECRECVSIZE     (MEDIA_EVENT_RECBASE + 4) //录制正常启动
#define MEDIA_EVENT_RECWAIT        	(MEDIA_EVENT_RECBASE + 5) //边下载边播放的等待
#define MEDIA_EVENT_RECRESUME       (MEDIA_EVENT_RECBASE + 6) //边下载边播放的等待

#define MEDIA_EVENT_PSIMONITOR_BASE 70
//#define MEDIA_EVENT_CA_FIND    MEDIA_EVENT_PSIMONITOR_BASE + 1  //psimonitor detect ca stream
//#define MEDIA_EVENT_NO_CA_FIND MEDIA_EVENT_PSIMONITOR_BASE + 2
//#define MEDIA_EVENT_GET_PMT    MEDIA_EVENT_PSIMONITOR_BASE + 3
#define MEDIA_EVENT_CA_STATUS		(MEDIA_EVENT_PSIMONITOR_BASE+4)		//IRDETO CA状态事件
#define MEDIA_EVENT_CA_RESUME		(MEDIA_EVENT_PSIMONITOR_BASE+5)		//IRDETO CA错误事件恢复
#define MEDIA_EVENT_CA_REBOOT		(MEDIA_EVENT_PSIMONITOR_BASE+6)		//IRDETO CA重启机顶盒事件

//louie add for hls muti kbps
#define MEDIA_EVENT_KBPS_HCHANGED    	(MEDIA_EVENT_PSIMONITOR_BASE+7)  //m3u8 kbps was changed before/head
#define MEDIA_EVENT_KBPS_BCHANGED    	(MEDIA_EVENT_PSIMONITOR_BASE+8)  //m3u8 kbps was changed back
//end

#define MEDIA_EVENT_ICCA_STATUS		(MEDIA_EVENT_PSIMONITOR_BASE+10)	//IRDETO CLOAKED CA重启机顶盒事件

//stephen sunniwell added this
#define MEDIA_EVENT_VIDEO_SIZE_CHANGE   (MEDIA_EVENT_PSIMONITOR_BASE+11)  //Video size was changed
#define MEDIA_EVENT_SEEK_COMPLETE   	(MEDIA_EVENT_PSIMONITOR_BASE+12)
#define MEDIA_EVENT_TIMEBASE_CHANGED    (MEDIA_EVENT_PSIMONITOR_BASE+13)  //timebase was changed
//end
#define MEDIA_EVENT_BUFFERING_UPDATE    (MEDIA_EVENT_PSIMONITOR_BASE+14)  //timebase was changed
#define MEDIA_EVENT_BUFFERING_START		(MEDIA_EVENT_PSIMONITOR_BASE+15)  //no buffer to decode
#define MEDIA_EVENT_BUFFERING_END		(MEDIA_EVENT_PSIMONITOR_BASE+16)  //revert to decode
#define MEDIA_EVENT_PREPARED			(MEDIA_EVENT_PSIMONITOR_BASE+17)  //related to android mediaplayer
#define MEDIA_EVENT_SNIFFED			    (MEDIA_EVENT_PSIMONITOR_BASE+18)  //stream info analyze finished


//定义多媒体播放状态
#define MEDIA_STATUS_IDLE           0 //空闲状态
#define MEDIA_STATUS_BEGINNING      1 //正在开始播放的状态
#define MEDIA_STATUS_BUFFERING      2 //缓冲状态
#define MEDIA_STATUS_PLAY           3 //正常播放状态
#define MEDIA_STATUS_PAUSE          4 //暂停状态
#define MEDIA_STATUS_TRICKPLAY      5 //快进快退状态
#define MEDIA_STATUS_ERROR          6 //出错状态
#define MEDIA_STATUS_STREAM2BEGIN   7 //流回到开始位置状态
#define MEDIA_STATUS_STREAM2END     8 //流到结束位置状态
#define MEDIA_STATUS_STOPING        9 //正在停止的状态
#define MEDIA_STATUS_SEEKING        10 //正在seek的状态


#define MERROR_BASE					0	
#define MERROR_NO_MEMORY			( -1 + MERROR_BASE) //内存不够
#define MERROR_SYSTEM_FATAL			( -2 + MERROR_BASE) //操作系统错误
#define MERROR_INVALID_URL			( -3 + MERROR_BASE) //不支持的URL	
#define MERROR_UNKNOWN_PROFILE		( -4 + MERROR_BASE) //profile格式不正确
#define MERROR_TARGET_NOTFOUND		( -5 + MERROR_BASE) //目标没有找到
#define MERROR_UNKNOWN_FORMAT		( -6 + MERROR_BASE) //未知的流格式					
#define MERROR_CREATESOCKET_FAILED	( -7 + MERROR_BASE) //创建SOCKET失败
#define MERROR_CONNECTSERVER_FAILED	( -8 + MERROR_BASE) //连接服务器失败
#define MERROR_DECODER_EXCEPTION	( -9 + MERROR_BASE) //decoder异常
#define MERROR_SESSION_EXCEPTION	(-10 + MERROR_BASE) //session 异常
#define MERROR_UNSUPPORT_SERVER		(-11 + MERROR_BASE) //不支持的视频服务器
#define MERROR_DISORDER_STATE		(-12 + MERROR_BASE) //播放状态混乱
#define MERROR_UNSUPPORT_PROTOCOL	(-13 + MERROR_BASE) //不支持的协议
#define MERROR_STATUS_INVALID_RANGE (-14 + MERROR_BASE) //分段播放使用, seek时间点不在分段内
#define MERROR_INVALID_CHAIN        (-15 + MERROR_BASE) //防盗链检测失败

#define MERROR_CANNT_SUPPORT_PLAY		(-16 + MERROR_BASE ) 
#define MERROR_CANNT_SUPPORT_SEEK		(-17 + MERROR_BASE ) 
#define MERROR_CANNT_SUPPORT_TRICK		(-18 + MERROR_BASE ) 

#define MERROR_RTSP_RST                  (-19 + MERROR_BASE )
#define MERROR_RTSP_INVALID              (-20 + MERROR_BASE ) 
#define MERROR_RTSP_CONNECT_LOST         (-21 + MERROR_BASE )
#define MERROR_RTSP_READ_FAILED          (-22 + MERROR_BASE )
#define MERROR_RTSP_SEND_FAILED          (-23 + MERROR_BASE )
#define MERROR_RTSP_CONNECT_STOPPED      (-24 + MERROR_BASE )                                                                                                      
#define MERROR_RTSP_CONNECTION_DIED      (-25 + MERROR_BASE )
#define MERROR_RTSP_UNSUPPORT_TIMESHIFT  (-26 + MERROR_BASE )

#define MERROR_CODEC_NOTSUPPORT     (-27 + MERROR_BASE ) //不支持解码

#define MERROR_FILE_NOTFOUND		(-28 + MERROR_BASE) //文件未找到
#define MERROR_FILE_IOERROR			(-29 + MERROR_BASE) //文件读写错误
#define MERROR_CODEC_NOLICENSE      (-30 + MERROR_BASE ) //权限没有打开

#define MERROR_DECRYPT_FAILED		(-100 + MERROR_BASE) //解密错误
#define MERROR_REACH_MAX_LICENSE	(-101 + MERROR_BASE) //到达最大用户数
#define MERROR_DRM_VERIMATRIX		(-102 + MERROR_BASE) //Verimatrix DRM错误
#define MERROR_DRM_MICROSOFT		(-103 + MERROR_BASE) //Microsoft DRM错误
#define MERROR_OTT_VERIMATRIX       (-104 + MERROR_BASE) //Verimatrix HLS错误
#define MERROR_MEDIA_NETWORK_BUSY   (-105 + MERROR_BASE)           

#define OTT_VERIMATRIX_START_FAILED  (-1041 + MERROR_BASE)
#define OTT_VERIMATRIX_GETKEY_FAILED (-1042 + MERROR_BASE)

//rtsp单播对接的媒体平台服务器名称
#define	MEDIA_SERVER_IDLE 0
#define	MEDIA_SERVER_HW   1 //华为平台
#define	MEDIA_SERVER_ZTE  2	//中兴平台
#define	MEDIA_SERVER_UT	  3	//UT平台
#define MEDIA_SERVER_FV   4 //FonsView平台


/**
 * @启动模式, 是否启动解码和录制
 */
typedef enum
{
	E_PLAYER_STARTMODE_NONE     = 0x0, /**<不启动任何销毁器*/
	E_PLAYER_STARTMODE_DEC      = 0x1, /**<启动解码器*/
	E_PLAYER_STARTMODE_REC      = 0x2, /**<启动录制器*/
	E_PLAYER_STARTMODE_TSREC    = 0x4, /**<时移录制器*/
	E_PLAYER_STARTMODE_MAX
} e_player_startmode;

/**
 * @视频显示模式
 */
typedef enum {
	SW_VIDEORENDER_NORMAL,		/**<正常模式,时钟同步之后才输出*/
	SW_VIDEORENDER_SLOWVIEW,	/**<视频慢放,等待同步后正常速率输出*/
	SW_VIDEORENDER_FREERUN,		/**<按照输出帧率输出图像*/
    SW_VIDEORENDER_BESTFIT,     /**<自适应模式, 介于normal和slow view之间, 在减小切台时间的同时, 尽可能地减小不同步时间 */
} sw_videorender_e;

/**
 * @声音输出模式
 */
typedef enum
{
    SW_SND_AUTO             = 0x00, /**<auto mode > **/
    SW_SNDHDMI_PCM 			= 0x10,	/**<非压缩数据输出到HDMI*/
    SW_SNDHDMI_PASSTHROUGH 	= 0x20,	/**<压缩数据输出到HDMI*/
    SW_SNDSPDIF_PCM 		= 0x01,	/**<非压缩数据输出到spdif*/
    SW_SNDSPDIF_PASSTHROUGH = 0x02,	/**<压缩数据输出到spdif*/
	SW_SNDSPDIF_NOOUTPUT    = 0x04, /**<spdif no output>**/
	SW_SNDHDMI_NOOUTPUT		= 0x40, /**<hdmi no output> **/
} sw_audiorender_e;

typedef enum _audiooutput
{
	SW_AUDIO_OUTPUT_MIXER = 0x1, 	/**<audio mixer*/
	SW_AUDIO_OUTPUT_SPDIF = 0x2, 	/**<spdif audio*/
	SW_AUDIO_OUTPUT_HDMI  = 0x4,  	/**<hdmi audio*/
	SW_AUDIO_OUTPUT_DAC	  = 0x8,   	/**<audio dac*/
	SW_AUDIO_OUTPUT_I2S	  = 0x10   	/**<i2s audio*/
} swaudiooutput_e;
	

/**
 * @声道定义
 */
typedef enum _audiochannel
{
	SW_AUDIO_CHANNEL_LR,	/**< 立体声 */
	SW_AUDIO_CHANNEL_L,		/**< 左声道 */
	SW_AUDIO_CHANNEL_R,		/**< 左声道 */
	SW_AUDIO_CHANNEL_RL		/**< 立体声(左右对换)*/
} swaudio_channel_e;

/**
 * @视频输出比例
 */
typedef enum _swaspectratio 
{
    SW_ASPECTRATIO_UNKNOWN,
    SW_ASPECTRATIO_4x3,     /**<4:3 */
    SW_ASPECTRATIO_16x9,    /**<16:9 */
    SW_ASPECTRATIO_221x1,   /**<2.21:1 */
    SW_ASPECTRATIO_15x9     /**<15:9 */
       
} swaspectratio_e;

/**
 * @视频内容输出模式
 */
typedef enum _contentmode
{
	SW_CONTENTMODE_ZOOM, 		/**<剪切源内容适应输出比例*/
	SW_CONTENTMODE_LETTERBOX, 	/**<保持源比例不变*/
	SW_CONTENTMODE_FULL,	    /**<按输出比例全屏输出*/
	SW_CONTENTMODE_PANSCAN		/**< 16:9片源在4:3显示器上切掉左右两条,4:3的片源在16:9显示器上切掉上下两条*/
} swcontentmode_e;

/**
 * @视频窗口模式
 */
typedef enum _pigmode
{
	SW_PIGMODE_PIG, 		/**<小窗口模式*/
	SW_PIGMODE_FULL, 		/**<全屏输出*/
} swpigmode_e;

/**
 * @录制类型
 */
typedef enum _rectype
{
	SW_RECTYPE_SOFT, /**<软件录制*/
	SW_RECTYPE_HARD, /**<硬件录制*/
	SW_RECTYPE_DVB   /**<DVB录制*/
}swrectype_e;


typedef struct _swplayer_config
{
    //用于确保每次开机使用不同的本地端口
    //每次开机递增1(确保断电关机后依然会递增1), 超过一定数目(如97)可重新从1开始
    int run_count;
    int dram_size; //dram_size大时内存池分配大些

    int player_mem_size;

    int main_player_count;
    int main_queue_size;
    int sqa_mem_size;
    int localtimeshift_size;

    int normal_player_count;
    int normal_queue_size;
    int sqa_mem_size_pip;

    int mosaic_player_count;
    int mosaic_queue_size;

    int frame_player_count;
    int frame_queue_size;

    int enable_ads;
    int enable_localtimeshift;

    int enable_sqa;
    int sqa_log_level;
    int enable_fec;
    int enable_vodret;
	int trace_log_level;
	
    int enable_fastcache;
    int transport_protocol;

    //for ctcctrl
    int timezone;
    int enable_burst;
    int enable_arq;
    int fec_port;

    int inner_stop_holdpic;


    int audio_min;
    int audio_max;

    char preferred_audiolang[4];
    char preferred_sublang[4];

    char drm_company_verimatrix[64];
    char drm_serverip_verimatrix[64];
    uint16_t drm_serverport_verimatrix;
    char drm_vks_verimatrix[64];

	char ott_verimatrix_serverip[128];
	char ott_verimatrix_company[128];
	int  ott_verimatrix_port;

    char lanip[32];
    char currentip[32];
    char mac[32];
    int pppoe_control;

    //打印状态信息的间隔, in ms
    uint32_t debug_print_interval;
	int NatInterval;
    int sqa_rtcp_port;
    int SQABufferHLevel;
    int SQABufferMLevel;
    int SQABufferLLevel;
    int RETSendPeriod;
    int RSRTimeOut;
    int SCNTimeOut;
    int ChanCacheTime;
    int RETDelayTime;
    int RETInterval;
    int DisorderTime;
    int RETEnable;
	char SqaConfigPara[1024];
	char currentipv6[64];
	char media_extpara[64];
	int force_use_vodret_type; 
	int sub_plane_order;
	char ott_download_rate[1024];
    int CTCFccFlag;
    int CTCSCNTimeOut;
    int CTCSCNLostTime;
    char stbid[33];
    int ott_rate_adaptation; /*0:关闭码率适配，1:打开码率适配*/
    int ott_firstrate;
    int ott_timeUnder;
    int ott_timeMid;
    int ott_interval;
    int ott_print_m3u8; 
    
    int ctc_video_seek_delay; //rtsp单播支持预缓冲,seek 后delay时间
    int ctc_use_pre_buffer;  //rtsp单播支持预缓冲
} swplayer_config_t;


#ifndef SUB_BUFS_NUM_MAX
#define SUB_BUFS_NUM_MAX 16
#endif
typedef struct _swsubtitle_render
{
    void (*draw_text)(void/*SPlane*/ *plane, uint8_t *bufs[SUB_BUFS_NUM_MAX], int bufs_len[SUB_BUFS_NUM_MAX]);
} swsubtitle_render_t;


#define MAX_PLAYERS_NUM 32


#ifdef __cplusplus
}
#endif

#endif // __SWPLAYERDEF_H__

