LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_CFLAGS += -DLINUX -DANDROID
LOCAL_PRELINK_MODULE := false

LOCAL_MODULE_TAGS := optional 

LOCAL_SRC_FILES:= \
	libSurface.cpp \

LOCAL_C_INCLUDES := \
	$(TOP)/frameworks/av/include/media \

LOCAL_MODULE := libSurface

include $(BUILD_STATIC_LIBRARY)
