#include <stdio.h>
#include <unistd.h>
#include <utils/Log.h>
#include <binder/IServiceManager.h>
#include <binder/IPCThreadState.h>
#include <gui/ISurfaceComposer.h>
#include <gui/Surface.h>
#include <gui/SurfaceComposerClient.h>
#include <ui/DisplayInfo.h>
#include <utils/RefBase.h>
#include <utils/Log.h>

using namespace android;
#include "libSurface.h"

#define LOG_TAG "libSurface"
#define PLANE_WIDTH 1280
#define PLANE_HEIGHT 720

static sp<Surface> surface = NULL;
static sp<SurfaceControl> sfcontrol = NULL;
static sp<Surface> videoSurface = NULL;
static sp<SurfaceControl> videoSfControl = NULL;
static sp<SurfaceComposerClient> client = NULL;
static DisplayInfo dinfo;

int sw_surface_init()
{
	sp<ProcessState> proc(ProcessState::self());
	ProcessState::self()->startThreadPool();

	client = new SurfaceComposerClient();
	if (client != NULL)
	{
		status_t status = client->getDisplayInfo(0, &dinfo);
		if (status != 0)
		{
			dinfo.w = PLANE_WIDTH;
			dinfo.h = PLANE_HEIGHT;
		}
	}

	sfcontrol = client->createSurface(String8("SurfaceView"), dinfo.w, dinfo.h, PIXEL_FORMAT_RGBA_8888,0);
	if(sfcontrol == NULL)
		return -1;
	SurfaceComposerClient::openGlobalTransaction();
	sfcontrol->setSize(dinfo.w, dinfo.h);
	sfcontrol->setPosition(1, 1);
	sfcontrol->setLayer(0x2000000);
	sfcontrol->show();
	SurfaceComposerClient::closeGlobalTransaction();
	surface = sfcontrol->getSurface();

	videoSfControl = client->createSurface(String8("SurfaceView"), dinfo.w, dinfo.h, PIXEL_FORMAT_RGBX_8888,0);
	if(videoSfControl == NULL)
		return -1;
	SurfaceComposerClient::openGlobalTransaction();
	videoSfControl->setSize(dinfo.w, dinfo.h);
	videoSfControl->setPosition(1, 1);
	videoSfControl->setLayer(0x2000000 - 1);
	videoSfControl->show();
	SurfaceComposerClient::closeGlobalTransaction();
	videoSurface = videoSfControl->getSurface();

	return 0;
}

void sw_surface_exit()
{
	client = NULL;
	surface = NULL;
	videoSurface = NULL;
	sfcontrol = NULL;
	videoSfControl = NULL;

	return ;
}

void* swgetSurface( )
{
	return surface.get();
}

void* swget_VideoSurface( )
{
	//return videoSurface.get();
	return (videoSurface->getSurfaceTexture()).get();
}
