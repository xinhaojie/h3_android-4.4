
#include <sw-deinterlace/deinterlaceSw.h>
#include <log.h>

DeinterlaceSw::DeinterlaceSw()
{
    awPP = NULL;
}

DeinterlaceSw::~DeinterlaceSw()
{
    if (awPP)
    {
        delete awPP;
    }
}

int DeinterlaceSw::init()
{
    if (awPP)
    {
        logw("already init...");
        return 0;
    }
    
	awPP = new CAWPostProcess();
	if (awPP == NULL)
	{
		loge("create CAWPostProcess2 failed");
		return -1;
	}

	logd("sw deinterlace init success...");
    return 0;
}
    
int DeinterlaceSw::reset()
{
    if (awPP)
    {
        delete awPP;
        awPP = NULL;
    }
    return init();
}

EPIXELFORMAT DeinterlaceSw::expectPixelFormat()
{
    return PIXEL_FORMAT_YV12;
}

int DeinterlaceSw::flag()
{
    return DE_INTERLACE_SW;
}

int DeinterlaceSw::process(VideoPicture *pPrePicture,
            VideoPicture *pCurPicture,
            VideoPicture *pOutPicture,
            int nField)
{
	bool ret = awPP->Process(pPrePicture, pCurPicture, pOutPicture, nField);
	if (!ret)
	{
		loge("pAWpp->Process failed");
		return -1;
	}
	else
	{
		// logd("pp");
	}

	pOutPicture->nPts = pCurPicture->nPts;

	return 0;
}

