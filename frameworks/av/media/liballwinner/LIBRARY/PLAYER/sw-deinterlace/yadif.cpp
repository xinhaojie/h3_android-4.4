#include "yadif.h"

#include <time.h>
#include <arm_neon.h>
#include <log.h>

#define USE_NEON 1
#define MORE_SPATIAL_CHECK 1 /* spatial interlacing check, shower */


#define MIN(a,b) ((a) > (b) ? (b) : (a))
#define MAX(a,b) ((a) < (b) ? (b) : (a))
#define ABS(a) ((a) > 0 ? (a) : (-(a)))
#define MIN3(a,b,c) MIN(MIN(a,b),c)
#define MAX3(a,b,c) MAX(MAX(a,b),c)

static int64_t systemTime()
{
    struct timespec t;
    t.tv_sec = t.tv_nsec = 0;
    clock_gettime(CLOCK_MONOTONIC, &t);
    return t.tv_sec*1000000000LL + t.tv_nsec;
}

static void filter_line_intrinsics(int mode, uint8_t *dst, const uint8_t *prev, const uint8_t *cur, const uint8_t *next, int w, int refs, int parity){
	int x;
	const uint8_t *prev2= parity ? prev : cur ;
	const uint8_t *next2= parity ? cur  : next;

	for (x = 0; x < w; x += 8) {

		/*int c= cur[-refs];
		int d= (prev2[0] + next2[0])>>1;
		int e= cur[+refs];
		int temporal_diff0= ABS(prev2[0] - next2[0]);
		int temporal_diff1=( ABS(prev[-refs] - c) + ABS(prev[+refs] - e) )>>1;
		int temporal_diff2=( ABS(next[-refs] - c) + ABS(next[+refs] - e) )>>1;
		int diff= MAX3(temporal_diff0>>1, temporal_diff1, temporal_diff2);
		int spatial_pred= (c+e)>>1;
		int spatial_score= ABS(cur[-refs-1] - cur[+refs-1]) + ABS(c-e)
						+ ABS(cur[-refs+1] - cur[+refs+1]) - 1;*/

		uint8x8_t c = vld1_u8((const uint8_t *)&cur[-refs]); /* v_cur_m_refs */
		__builtin_prefetch(&cur[-refs+8], 0);
		uint8x8_t v_prev2 = vld1_u8((const uint8_t *)prev2);
		uint8x8_t v_next2 = vld1_u8((const uint8_t *)next2);
		uint8x8_t d = vhadd_u8(v_prev2, v_next2);
		uint8x8_t e = vld1_u8((const uint8_t *)&cur[+refs]); /* v_cur_p_refs */
		__builtin_prefetch(&cur[+refs+8], 0);

#define v_cur_m_refs c
#define v_cur_p_refs e

		uint8x8_t temp_diff0 = vabd_u8(v_prev2, v_next2);
		temp_diff0 = vshr_n_u8(temp_diff0, 1);
		uint8x8_t v_prev_m_refs = vld1_u8((const uint8_t *)&prev[-refs]);
		__builtin_prefetch(&prev[-refs+8], 0);
		uint8x8_t v_prev_p_refs = vld1_u8((const uint8_t *)&prev[+refs]);
		__builtin_prefetch(&prev[+refs+8], 0);
		uint8x8_t temp_diff1 = vhadd_u8(vabd_u8(v_prev_m_refs, c), vabd_u8(v_prev_p_refs, e));
		uint8x8_t v_next_m_refs = vld1_u8((const uint8_t *)&next[-refs]);
		__builtin_prefetch(&next[-refs+8], 0);
		uint8x8_t v_next_p_refs = vld1_u8((const uint8_t *)&next[+refs]);
		__builtin_prefetch(&next[+refs+8], 0);
		uint8x8_t temp_diff2 = vhadd_u8(vabd_u8(v_next_m_refs, c), vabd_u8(v_next_p_refs, e));
		uint8x8_t diff = vmax_u8(vmax_u8(temp_diff0, temp_diff1), temp_diff2);

		uint8x8_t spatial_pred = vhadd_u8(c, e);

		uint8x8_t v_cur_m_refs_m_1 = vld1_u8((const uint8_t *)&cur[-refs-1]);
		uint8x8_t v_cur_m_refs_p_1 = vld1_u8((const uint8_t *)&cur[-refs+1]);
		uint8x8_t v_cur_p_refs_m_1 = vld1_u8((const uint8_t *)&cur[+refs-1]);
		uint8x8_t v_cur_p_refs_p_1 = vld1_u8((const uint8_t *)&cur[+refs+1]);
		uint16x8_t spatial_score = vabdl_u8(v_cur_m_refs_m_1, v_cur_p_refs_m_1);
		spatial_score = vabal_u8(spatial_score, c, e);
		spatial_score = vabal_u8(spatial_score, v_cur_m_refs_p_1, v_cur_p_refs_p_1);
		uint16x8_t v_m_1 = vdupq_n_u16(1);
		spatial_score = vsubq_u16(spatial_score, v_m_1);

		uint8x8_t v_cur_m_refs_m_2 = vld1_u8((const uint8_t *)&cur[-refs-2]);
		uint8x8_t v_cur_m_refs_m_3 = vld1_u8((const uint8_t *)&cur[-refs-3]);
		uint8x8_t v_cur_m_refs_p_2 = vld1_u8((const uint8_t *)&cur[-refs+2]);
		uint8x8_t v_cur_m_refs_p_3 = vld1_u8((const uint8_t *)&cur[-refs+3]);

		uint8x8_t v_cur_p_refs_m_2 = vld1_u8((const uint8_t *)&cur[+refs-2]);
		uint8x8_t v_cur_p_refs_m_3 = vld1_u8((const uint8_t *)&cur[+refs-3]);
		uint8x8_t v_cur_p_refs_p_2 = vld1_u8((const uint8_t *)&cur[+refs+2]);
		uint8x8_t v_cur_p_refs_p_3 = vld1_u8((const uint8_t *)&cur[+refs+3]);

		uint16x8_t status;
		uint16x8_t status2;
		uint8x8_t status8x8;

#if 1
		/*int score= ABS(cur[-refs-1+ j] - cur[+refs-1- j])
				 + ABS(cur[-refs  + j] - cur[+refs	- j])
				 + ABS(cur[-refs+1+ j] - cur[+refs+1- j]);
		if(score < spatial_score){
			spatial_score= score;
			spatial_pred= (cur[-refs  + j] + cur[+refs	- j])>>1;
		}
		CHECK(-1) CHECK(-2)
		CHECK( 1) CHECK( 2)*/

		// j=-1
		uint16x8_t score = vabdl_u8(v_cur_m_refs_m_2, v_cur_p_refs);
		score = vabal_u8(score, v_cur_m_refs_m_1, v_cur_p_refs_p_1);
		score = vabal_u8(score, v_cur_m_refs, v_cur_p_refs_p_2);
		uint8x8_t spatial_pred_1 = vhadd_u8(v_cur_m_refs_m_1, v_cur_p_refs_p_1);
		status = vcgtq_u16(spatial_score, score);
		spatial_score = vbslq_u16(status, score, spatial_score);
		status8x8 = vmovn_u16(status);
		spatial_pred = vbsl_u8(status8x8, spatial_pred_1, spatial_pred);

		// j=-2
		score = vabdl_u8(v_cur_m_refs_m_3, v_cur_p_refs_p_1);
		score = vabal_u8(score, v_cur_m_refs_m_2, v_cur_p_refs_p_2);
		score = vabal_u8(score, v_cur_m_refs_m_1, v_cur_p_refs_p_3);
		spatial_pred_1 = vhadd_u8(v_cur_m_refs_m_2, v_cur_p_refs_p_2);
		status2 = vcgtq_u16(spatial_score, score);
		status = vandq_u16(status, status2);
		spatial_score = vbslq_u16(status, score, spatial_score);
		status8x8 = vmovn_u16(status);
		spatial_pred = vbsl_u8(status8x8, spatial_pred_1, spatial_pred);

		// j=1
		score = vabdl_u8(v_cur_m_refs, v_cur_p_refs_m_2);
		score = vabal_u8(score, v_cur_m_refs_p_1, v_cur_p_refs_m_1);
		score = vabal_u8(score, v_cur_m_refs_p_2, v_cur_p_refs);
		spatial_pred_1 = vhadd_u8(v_cur_m_refs_p_1, v_cur_p_refs_m_1);
		status = vcgtq_u16(spatial_score, score);
		spatial_score = vbslq_u16(status, score, spatial_score);
		status8x8 = vmovn_u16(status);
		spatial_pred = vbsl_u8(status8x8, spatial_pred_1, spatial_pred);

		// j=2
		score = vabdl_u8(v_cur_m_refs_p_1, v_cur_p_refs_m_3);
		score = vabal_u8(score, v_cur_m_refs_p_2, v_cur_p_refs_m_2);
		score = vabal_u8(score, v_cur_m_refs_p_3, v_cur_p_refs_m_1);
		spatial_pred_1 = vhadd_u8(v_cur_m_refs_p_2, v_cur_p_refs_m_2);
		status2 = vcgtq_u16(spatial_score, score);
		status = vandq_u16(status, status2);
		spatial_score = vbslq_u16(status, score, spatial_score);
		status8x8 = vmovn_u16(status);
		spatial_pred = vbsl_u8(status8x8, spatial_pred_1, spatial_pred);
#endif

		/*if(mode<2){
			int b= (prev2[-2*refs] + next2[-2*refs])>>1;
			int f= (prev2[+2*refs] + next2[+2*refs])>>1;
			int max= MAX3(d-e, d-c, MIN(b-c, f-e));
			int min= MIN3(d-e, d-c, MAX(b-c, f-e));
			diff= MAX3(diff, min, -max);
		}*/

		if (mode < 2)
		{
			uint8x8_t v_prev2_m_refs_m_refs = vld1_u8((const uint8_t *)&prev2[-2*refs]);
			uint8x8_t v_prev2_p_refs_p_refs = vld1_u8((const uint8_t *)&prev2[+2*refs]);
			uint8x8_t v_next2_m_refs_m_refs = vld1_u8((const uint8_t *)&next2[-2*refs]);
			uint8x8_t v_next2_p_refs_p_refs = vld1_u8((const uint8_t *)&next2[+2*refs]);
			uint16x8_t b = vaddl_u8(v_prev2_m_refs_m_refs, v_next2_m_refs_m_refs);
			b = vshrq_n_u16(b, 1);
			uint16x8_t f = vaddl_u8(v_prev2_p_refs_p_refs, v_next2_p_refs_p_refs);
			f = vshrq_n_u16(f, 1);
			int16x8_t b_l = vreinterpretq_s16_u16(b);
			int16x8_t f_l = vreinterpretq_s16_u16(f);
			uint16x8_t c_ul = vmovl_u8(c);
			int16x8_t c_l = vreinterpretq_s16_u16(c_ul);
			uint16x8_t d_ul = vmovl_u8(d);
			int16x8_t d_l = vreinterpretq_s16_u16(d_ul);
			uint16x8_t e_ul = vmovl_u8(e);
			int16x8_t e_l = vreinterpretq_s16_u16(e_ul);
			int16x8_t d_m_e = vsubq_s16(d_l, e_l);
			int16x8_t d_m_c = vsubq_s16(d_l, c_l);
			int16x8_t b_m_c = vsubq_s16(b_l, c_l);
			int16x8_t f_m_e = vsubq_s16(f_l, e_l);
			int16x8_t max = vmaxq_s16(vmaxq_s16(d_m_e, d_m_c), vminq_s16(b_m_c, f_m_e));
			int16x8_t min = vminq_s16(vminq_s16(d_m_e, d_m_c), vmaxq_s16(b_m_c, f_m_e));
			uint16x8_t diff_ul = vmovl_u8(diff);
			int16x8_t diff_l = vreinterpretq_s16_u16(diff_ul);
			diff_l = vmaxq_s16(vmaxq_s16(diff_l, min), vnegq_s16(max));
			diff_ul = vreinterpretq_u16_s16(diff_l);
			diff = vmovn_u16(diff_ul);
		}

		/*if(spatial_pred > d + diff)
			spatial_pred = d + diff;
		else if(spatial_pred < d - diff)
			spatial_pred = d - diff;*/

		uint16x8_t spatial_pred_ul = vmovl_u8(spatial_pred);
		uint16x8_t d_ul = vmovl_u8(d);
		uint16x8_t diff_ul = vmovl_u8(diff);
		int16x8_t spatial_pred_l = vreinterpretq_s16_u16(spatial_pred_ul);
		int16x8_t d_l = vreinterpretq_s16_u16(d_ul);
		int16x8_t diff_l = vreinterpretq_s16_u16(diff_ul);

		int16x8_t d_p_diff_l = vaddq_s16(d_l, diff_l);
		int16x8_t d_m_diff_l = vsubq_s16(d_l, diff_l);

		status = vcgtq_s16(spatial_pred_l, d_p_diff_l);
		spatial_pred_l = vbslq_s16(status, d_p_diff_l, spatial_pred_l);
		status = vcltq_s16(spatial_pred_l, d_m_diff_l);
		spatial_pred_l = vbslq_s16(status, d_m_diff_l, spatial_pred_l);
		spatial_pred_ul = vreinterpretq_u16_s16(spatial_pred_l);
		spatial_pred = vmovn_u16(spatial_pred_ul);

		/* dst[0] = spatial_pred; */
		vst1_u8(dst, spatial_pred);

		dst+=8;
        cur+=8;
        prev+=8;
        next+=8;
        prev2+=8;
        next2+=8;
	}
}

// assume:
//	  prev2=prev
//	  next2=cur
//	  next=cur
static void filter_line_intrinsics_debug(int mode, uint8_t *dst, const uint8_t *prev, const uint8_t *cur, const uint8_t *next, int w, int refs, int parity){
	int x;
	const uint8_t *prev2= parity ? prev : cur ;
	const uint8_t *next2= parity ? cur  : next;

	for (x = 0; x < w; x += 8) {

		/*int c= cur[-refs];
		int d= (prev2[0] + next2[0])>>1;
		int e= cur[+refs];
		int temporal_diff0= ABS(prev2[0] - next2[0]);
		int temporal_diff1=( ABS(prev[-refs] - c) + ABS(prev[+refs] - e) )>>1;
		int temporal_diff2=( ABS(next[-refs] - c) + ABS(next[+refs] - e) )>>1;
		int diff= MAX3(temporal_diff0>>1, temporal_diff1, temporal_diff2);
		int spatial_pred= (c+e)>>1;
		int spatial_score= ABS(cur[-refs-1] - cur[+refs-1]) + ABS(c-e)
						+ ABS(cur[-refs+1] - cur[+refs+1]) - 1;*/

		uint8x8_t c = vld1_u8((const uint8_t *)&cur[-refs]); /* v_cur_m_refs */
		//__builtin_prefetch(&cur[-refs+8], 0);
		uint8x8_t v_prev = vld1_u8((const uint8_t *)prev);
		uint8x8_t v_cur = vld1_u8((const uint8_t *)cur);

#define v_prev2 v_prev
#define v_next2 v_cur

		uint8x8_t d = vhadd_u8(v_prev2, v_next2);
		uint8x8_t e = vld1_u8((const uint8_t *)&cur[+refs]); /* v_cur_p_refs */
		//__builtin_prefetch(&cur[+refs+8], 0);

#define v_cur_m_refs c
#define v_cur_p_refs e

		uint8x8_t temp_diff0 = vabd_u8(v_prev2, v_next2);
		temp_diff0 = vshr_n_u8(temp_diff0, 1);
		uint8x8_t v_prev_m_refs = vld1_u8((const uint8_t *)&prev[-refs]);
		//__builtin_prefetch(&prev[-refs+8], 0);
		uint8x8_t v_prev_p_refs = vld1_u8((const uint8_t *)&prev[+refs]);
		//__builtin_prefetch(&prev[+refs+8], 0);
		uint8x8_t temp_diff1 = vhadd_u8(vabd_u8(v_prev_m_refs, c), vabd_u8(v_prev_p_refs, e));

#define v_next_m_refs v_cur_m_refs
#define v_next_p_refs v_cur_p_refs

		//uint8x8_t temp_diff2 = vhadd_u8(vabd_u8(v_next_m_refs, c), vabd_u8(v_next_p_refs, e));
		//uint8x8_t diff = vmax_u8(vmax_u8(temp_diff0, temp_diff1), temp_diff2);
		// temp_diff2==0, because next=cur
		uint8x8_t diff = vmax_u8(temp_diff0, temp_diff1);

		uint8x8_t spatial_pred = vhadd_u8(c, e);

		uint8x8_t v_cur_m_refs_m_1 = vld1_u8((const uint8_t *)&cur[-refs-1]);
		uint8x8_t v_cur_m_refs_p_1 = vld1_u8((const uint8_t *)&cur[-refs+1]);
		uint8x8_t v_cur_p_refs_m_1 = vld1_u8((const uint8_t *)&cur[+refs-1]);
		uint8x8_t v_cur_p_refs_p_1 = vld1_u8((const uint8_t *)&cur[+refs+1]);
		uint16x8_t spatial_score = vabdl_u8(v_cur_m_refs_m_1, v_cur_p_refs_m_1);
		spatial_score = vabal_u8(spatial_score, c, e);
		spatial_score = vabal_u8(spatial_score, v_cur_m_refs_p_1, v_cur_p_refs_p_1);
		uint16x8_t v_m_1 = vdupq_n_u16(1);
		spatial_score = vsubq_u16(spatial_score, v_m_1);

		uint8x8_t v_cur_m_refs_m_2 = vld1_u8((const uint8_t *)&cur[-refs-2]);
		uint8x8_t v_cur_m_refs_m_3 = vld1_u8((const uint8_t *)&cur[-refs-3]);
		uint8x8_t v_cur_m_refs_p_2 = vld1_u8((const uint8_t *)&cur[-refs+2]);
		uint8x8_t v_cur_m_refs_p_3 = vld1_u8((const uint8_t *)&cur[-refs+3]);

		uint8x8_t v_cur_p_refs_m_2 = vld1_u8((const uint8_t *)&cur[+refs-2]);
		uint8x8_t v_cur_p_refs_m_3 = vld1_u8((const uint8_t *)&cur[+refs-3]);
		uint8x8_t v_cur_p_refs_p_2 = vld1_u8((const uint8_t *)&cur[+refs+2]);
		uint8x8_t v_cur_p_refs_p_3 = vld1_u8((const uint8_t *)&cur[+refs+3]);

		uint16x8_t status;
		uint16x8_t status2;
		uint8x8_t status8x8;

		/*int score= ABS(cur[-refs-1+ j] - cur[+refs-1- j])
				 + ABS(cur[-refs  + j] - cur[+refs	- j])
				 + ABS(cur[-refs+1+ j] - cur[+refs+1- j]);
		if(score < spatial_score){
			spatial_score= score;
			spatial_pred= (cur[-refs  + j] + cur[+refs	- j])>>1;
		}
		CHECK(-1) CHECK(-2)
		CHECK( 1) CHECK( 2)*/

		// j=-1
		uint16x8_t score = vabdl_u8(v_cur_m_refs_m_2, v_cur_p_refs);
		score = vabal_u8(score, v_cur_m_refs_m_1, v_cur_p_refs_p_1);
		score = vabal_u8(score, v_cur_m_refs, v_cur_p_refs_p_2);
		uint8x8_t spatial_pred_1 = vhadd_u8(v_cur_m_refs_m_1, v_cur_p_refs_p_1);
		status = vcgtq_u16(spatial_score, score);
		spatial_score = vbslq_u16(status, score, spatial_score);
		status8x8 = vmovn_u16(status);
		spatial_pred = vbsl_u8(status8x8, spatial_pred_1, spatial_pred);

		// j=-2
		score = vabdl_u8(v_cur_m_refs_m_3, v_cur_p_refs_p_1);
		score = vabal_u8(score, v_cur_m_refs_m_2, v_cur_p_refs_p_2);
		score = vabal_u8(score, v_cur_m_refs_m_1, v_cur_p_refs_p_3);
		spatial_pred_1 = vhadd_u8(v_cur_m_refs_m_2, v_cur_p_refs_p_2);
		status2 = vcgtq_u16(spatial_score, score);
		status = vandq_u16(status, status2);
		spatial_score = vbslq_u16(status, score, spatial_score);
		status8x8 = vmovn_u16(status);
		spatial_pred = vbsl_u8(status8x8, spatial_pred_1, spatial_pred);

		// j=1
		score = vabdl_u8(v_cur_m_refs, v_cur_p_refs_m_2);
		score = vabal_u8(score, v_cur_m_refs_p_1, v_cur_p_refs_m_1);
		score = vabal_u8(score, v_cur_m_refs_p_2, v_cur_p_refs);
		spatial_pred_1 = vhadd_u8(v_cur_m_refs_p_1, v_cur_p_refs_m_1);
		status = vcgtq_u16(spatial_score, score);
		spatial_score = vbslq_u16(status, score, spatial_score);
		status8x8 = vmovn_u16(status);
		spatial_pred = vbsl_u8(status8x8, spatial_pred_1, spatial_pred);

		// j=2
		score = vabdl_u8(v_cur_m_refs_p_1, v_cur_p_refs_m_3);
		score = vabal_u8(score, v_cur_m_refs_p_2, v_cur_p_refs_m_2);
		score = vabal_u8(score, v_cur_m_refs_p_3, v_cur_p_refs_m_1);
		spatial_pred_1 = vhadd_u8(v_cur_m_refs_p_2, v_cur_p_refs_m_2);
		status2 = vcgtq_u16(spatial_score, score);
		status = vandq_u16(status, status2);
		spatial_score = vbslq_u16(status, score, spatial_score);
		status8x8 = vmovn_u16(status);
		spatial_pred = vbsl_u8(status8x8, spatial_pred_1, spatial_pred);

		/*if(mode<2){
			int b= (prev2[-2*refs] + next2[-2*refs])>>1;
			int f= (prev2[+2*refs] + next2[+2*refs])>>1;
			int max= MAX3(d-e, d-c, MIN(b-c, f-e));
			int min= MIN3(d-e, d-c, MAX(b-c, f-e));
			diff= MAX3(diff, min, -max);
		}*/

		if (mode < 2)
		{
			uint8x8_t v_prev_m_refs_m_refs = vld1_u8((const uint8_t *)&prev[-2*refs]);
			uint8x8_t v_prev_p_refs_p_refs = vld1_u8((const uint8_t *)&prev[+2*refs]);
			uint8x8_t v_cur_m_refs_m_refs = vld1_u8((const uint8_t *)&cur[-2*refs]);
			uint8x8_t v_cur_p_refs_p_refs = vld1_u8((const uint8_t *)&cur[+2*refs]);

#define v_prev2_m_refs_m_refs v_prev_m_refs_m_refs
#define v_prev2_p_refs_p_refs v_prev_p_refs_p_refs
#define v_next2_m_refs_m_refs v_cur_m_refs_m_refs
#define v_next2_p_refs_p_refs v_cur_p_refs_p_refs

			uint16x8_t b = vaddl_u8(v_prev2_m_refs_m_refs, v_next2_m_refs_m_refs);
			b = vshrq_n_u16(b, 1);
			uint16x8_t f = vaddl_u8(v_prev2_p_refs_p_refs, v_next2_p_refs_p_refs);
			f = vshrq_n_u16(f, 1);
			int16x8_t b_l = vreinterpretq_s16_u16(b);
			int16x8_t f_l = vreinterpretq_s16_u16(f);
			uint16x8_t c_ul = vmovl_u8(c);
			int16x8_t c_l = vreinterpretq_s16_u16(c_ul);
			uint16x8_t d_ul = vmovl_u8(d);
			int16x8_t d_l = vreinterpretq_s16_u16(d_ul);
			uint16x8_t e_ul = vmovl_u8(e);
			int16x8_t e_l = vreinterpretq_s16_u16(e_ul);
			int16x8_t d_m_e = vsubq_s16(d_l, e_l);
			int16x8_t d_m_c = vsubq_s16(d_l, c_l);
			int16x8_t b_m_c = vsubq_s16(b_l, c_l);
			int16x8_t f_m_e = vsubq_s16(f_l, e_l);
			int16x8_t max = vmaxq_s16(vmaxq_s16(d_m_e, d_m_c), vminq_s16(b_m_c, f_m_e));
			int16x8_t min = vminq_s16(vminq_s16(d_m_e, d_m_c), vmaxq_s16(b_m_c, f_m_e));
			uint16x8_t diff_ul = vmovl_u8(diff);
			int16x8_t diff_l = vreinterpretq_s16_u16(diff_ul);
			diff_l = vmaxq_s16(vmaxq_s16(diff_l, min), vnegq_s16(max));
			diff_ul = vreinterpretq_u16_s16(diff_l);
			diff = vmovn_u16(diff_ul);
		}

		/*if(spatial_pred > d + diff)
			spatial_pred = d + diff;
		else if(spatial_pred < d - diff)
			spatial_pred = d - diff;*/

		uint16x8_t spatial_pred_ul = vmovl_u8(spatial_pred);
		uint16x8_t d_ul = vmovl_u8(d);
		uint16x8_t diff_ul = vmovl_u8(diff);
		int16x8_t spatial_pred_l = vreinterpretq_s16_u16(spatial_pred_ul);
		int16x8_t d_l = vreinterpretq_s16_u16(d_ul);
		int16x8_t diff_l = vreinterpretq_s16_u16(diff_ul);

		int16x8_t d_p_diff_l = vaddq_s16(d_l, diff_l);
		int16x8_t d_m_diff_l = vsubq_s16(d_l, diff_l);

		status = vcgtq_s16(spatial_pred_l, d_p_diff_l);
		spatial_pred_l = vbslq_s16(status, d_p_diff_l, spatial_pred_l);
		status = vcltq_s16(spatial_pred_l, d_m_diff_l);
		spatial_pred_l = vbslq_s16(status, d_m_diff_l, spatial_pred_l);
		spatial_pred_ul = vreinterpretq_u16_s16(spatial_pred_l);
		spatial_pred = vmovn_u16(spatial_pred_ul);

		/* dst[0] = spatial_pred; */
		vst1_u8(dst, spatial_pred);

		dst+=8;
        cur+=8;
        prev+=8;
        //next+=8;
        //prev2+=8;
        //next2+=8;
	}
}

/* original method from mplayer */
static void filter_line_c(int mode, uint8_t *dst, const uint8_t *prev, const uint8_t *cur, const uint8_t *next, int w, int refs, int parity){
    int x;
    const uint8_t *prev2= parity ? prev : cur ;
    const uint8_t *next2= parity ? cur  : next;
    for(x=0; x<w; x++){
        int c= cur[-refs];
        int d= (prev2[0] + next2[0])>>1;
        int e= cur[+refs];
        int temporal_diff0= ABS(prev2[0] - next2[0]);
        int temporal_diff1=( ABS(prev[-refs] - c) + ABS(prev[+refs] - e) )>>1;
        int temporal_diff2=( ABS(next[-refs] - c) + ABS(next[+refs] - e) )>>1;
        int diff= MAX3(temporal_diff0>>1, temporal_diff1, temporal_diff2);

        int spatial_pred= (c+e)>>1;
        int spatial_score= ABS(cur[-refs-1] - cur[+refs-1]) + ABS(c-e)
                         + ABS(cur[-refs+1] - cur[+refs+1]) - 1;

#define CHECK(j)\
    {   int score= ABS(cur[-refs-1+ j] - cur[+refs-1- j])\
                 + ABS(cur[-refs  + j] - cur[+refs  - j])\
                 + ABS(cur[-refs+1+ j] - cur[+refs+1- j]);\
        if(score < spatial_score){\
            spatial_score= score;\
            spatial_pred= (cur[-refs  + j] + cur[+refs  - j])>>1;\

        CHECK(-1) CHECK(-2) }} }}
        CHECK( 1) CHECK( 2) }} }}

        if(mode<2){
            int b= (prev2[-2*refs] + next2[-2*refs])>>1;
            int f= (prev2[+2*refs] + next2[+2*refs])>>1;
#if 0
            int a= cur[-3*refs];
            int g= cur[+3*refs];
            int max= MAX3(d-e, d-c, MIN3(MAX(b-c,f-e),MAX(b-c,b-a),MAX(f-g,f-e)) );
            int min= MIN3(d-e, d-c, MAX3(MIN(b-c,f-e),MIN(b-c,b-a),MIN(f-g,f-e)) );
#else
            int max= MAX3(d-e, d-c, MIN(b-c, f-e));
            int min= MIN3(d-e, d-c, MAX(b-c, f-e));
#endif

            diff= MAX3(diff, min, -max);
        }

        if(spatial_pred > d + diff)
           spatial_pred = d + diff;
        else if(spatial_pred < d - diff)
           spatial_pred = d - diff;

        dst[0] = spatial_pred;

        dst++;
        cur++;
        prev++;
        next++;
        prev2++;
        next2++;
    }
}

#if USE_NEON
static void interpolate(uint8_t *dst, const uint8_t *cur0,  const uint8_t *cur2, int w)
{
    int x;
    for (x=0; x < w; x+=8) {
		uint8x8_t v_cur0 = vld1_u8(cur0+x);
		uint8x8_t v_cur2 = vld1_u8(cur2+x);
		uint8x8_t ave = vrhadd_u8(v_cur0, v_cur2);
		vst1_u8(dst+x, ave);
    }
}
#else
static void interpolate(uint8_t *dst, const uint8_t *cur0,  const uint8_t *cur2, int w)
{
    int x;
	for (x=0; x<w; x++) {
		dst[x] = (cur0[x] + cur2[x] + 1)>>1; // simple average
	}
}
#endif

void yadif_plane(uint8_t *dst, int dst_stride, const uint8_t *prev0, const uint8_t *cur0, const uint8_t *next0, int refs, int w, int h, int parity, int tff){

	int y;
	int mode;
	void (*filter_line)(int mode, uint8_t *dst, const uint8_t *prev, const uint8_t *cur, const uint8_t *next, int w, int refs, int parity);

#if USE_NEON
	filter_line = filter_line_intrinsics;
#else
	filter_line = filter_line_c;
#endif

#if MORE_SPATIAL_CHECK
	mode = 0;
#else
	mode = 3;
#endif

	int64_t filter_plane_time = systemTime();;

    y=0;
    if(((y ^ parity) & 1)){
        memcpy(dst, cur0 + refs, w);// duplicate 1
    }else{
        memcpy(dst, cur0, w);
    }
    y=1;
    if(((y ^ parity) & 1)){
        interpolate(dst + dst_stride, cur0, cur0 + refs*2, w);   // interpolate 0 and 2
    }else{
        memcpy(dst + dst_stride, cur0 + refs, w); // copy original
    }

    for(y=2; y<h-2; y++){
        if(((y ^ parity) & 1)){
            const uint8_t *prev= prev0 + y*refs;
            const uint8_t *cur = cur0 + y*refs;
            const uint8_t *next= next0 + y*refs;
            uint8_t *dst2= dst + y*dst_stride;
            filter_line(mode, dst2, prev, cur, next, w, refs, (parity ^ tff));
        }
		else{
            memcpy(dst + y*dst_stride, cur0 + y*refs, w); // copy original
        }
    }

    y=h-2;
    if(((y ^ parity) & 1)){
        interpolate(dst + (h-2)*dst_stride, cur0 + (h-3)*refs, cur0 + (h-1)*refs, w);   // interpolate h-3 and h-1
    }else{
        memcpy(dst + (h-2)*dst_stride, cur0 + (h-2)*refs, w); // copy original
    }
    y=h-1;
    if(((y ^ parity) & 1)){
        memcpy(dst + (h-1)*dst_stride, cur0 + (h-2)*refs, w); // duplicate h-2
    }else{
        memcpy(dst + (h-1)*dst_stride, cur0 + (h-1)*refs, w); // copy original
    }

	filter_plane_time = systemTime()-filter_plane_time;
	//logd("filter<%d> %lld", w, filter_plane_time/1000);
}

