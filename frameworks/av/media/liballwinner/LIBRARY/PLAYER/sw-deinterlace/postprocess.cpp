#define LOG_TAG "aw_pp"
// #define LOG_NDEBUG 0
#include <cutils/log.h>
#include "postprocess.h"
#include "yadif.h"

#define pp_logd	ALOGD
#define pp_loge	ALOGE

#define ABS(a) ((a) > 0 ? (a) : (-(a)))
#define MIN(a,b) ((a) > (b) ? (b) : (a))
#define MAX(a,b) ((a) < (b) ? (b) : (a))
#define ALIGN(x, a) (((x)+(a)-1)&~((a)-1))
#define BLOCK_SIZE 8

#define F_LOG 	//pp_logd("%s, line:%d", __FUNCTION__, __LINE__);

static inline void blockCopy(uint8_t dst[], int dstStride, const uint8_t src[], int srcStride)
{
	int i;
	for(i=0; i<8; i++)
        memcpy( &(dst[dstStride*i]),
                &(src[srcStride*i]), BLOCK_SIZE);
}

static inline void duplicate(uint8_t src[], int stride)
{
    int i;
    uint8_t *p=src;
    for(i=0; i<5; i++){
        p-= stride;
        memcpy(p, src, 8);
    }
}

#define clip_uint8 clip_uint8_arm
static inline unsigned clip_uint8_arm(int a)
{
    unsigned x;
    __asm__ ("usat %0, #8,  %1" : "=r"(x) : "r"(a));
    return x;
}

static inline void deInterlaceBlendLinear(uint8_t src[], int stride, uint8_t *tmp)
{
    int a, b, c, x;
    src+= 4*stride;

    for(x=0; x<2; x++){
        a= *(uint32_t*)&tmp[stride*0];
        b= *(uint32_t*)&src[stride*0];
        c= *(uint32_t*)&src[stride*1];
        a= (a&c) + (((a^c)&0xFEFEFEFEUL)>>1);
        *(uint32_t*)&src[stride*0]= (a|b) - (((a^b)&0xFEFEFEFEUL)>>1);

        a= *(uint32_t*)&src[stride*2];
        b= (a&b) + (((a^b)&0xFEFEFEFEUL)>>1);
        *(uint32_t*)&src[stride*1]= (c|b) - (((c^b)&0xFEFEFEFEUL)>>1);

        b= *(uint32_t*)&src[stride*3];
        c= (b&c) + (((b^c)&0xFEFEFEFEUL)>>1);
        *(uint32_t*)&src[stride*2]= (c|a) - (((c^a)&0xFEFEFEFEUL)>>1);

        c= *(uint32_t*)&src[stride*4];
        a= (a&c) + (((a^c)&0xFEFEFEFEUL)>>1);
        *(uint32_t*)&src[stride*3]= (a|b) - (((a^b)&0xFEFEFEFEUL)>>1);

        a= *(uint32_t*)&src[stride*5];
        b= (a&b) + (((a^b)&0xFEFEFEFEUL)>>1);
        *(uint32_t*)&src[stride*4]= (c|b) - (((c^b)&0xFEFEFEFEUL)>>1);

        b= *(uint32_t*)&src[stride*6];
        c= (b&c) + (((b^c)&0xFEFEFEFEUL)>>1);
        *(uint32_t*)&src[stride*5]= (c|a) - (((c^a)&0xFEFEFEFEUL)>>1);

        c= *(uint32_t*)&src[stride*7];
        a= (a&c) + (((a^c)&0xFEFEFEFEUL)>>1);
        *(uint32_t*)&src[stride*6]= (a|b) - (((a^b)&0xFEFEFEFEUL)>>1);

        a= *(uint32_t*)&src[stride*8];
        b= (a&b) + (((a^b)&0xFEFEFEFEUL)>>1);
        *(uint32_t*)&src[stride*7]= (c|b) - (((c^b)&0xFEFEFEFEUL)>>1);

        *(uint32_t*)&tmp[stride*0]= c;
        src += 4;
        tmp += 4;
    }
}

static inline void deInterlaceInterpolateCubic(uint8_t src[], int stride)
{
    int x;
    src+= stride*3;
    for(x=0; x<8; x++){
        src[stride*3] = clip_uint8((-src[0]        + 9*src[stride*2] + 9*src[stride*4] - src[stride*6])>>4);
        src[stride*5] = clip_uint8((-src[stride*2] + 9*src[stride*4] + 9*src[stride*6] - src[stride*8])>>4);
        src[stride*7] = clip_uint8((-src[stride*4] + 9*src[stride*6] + 9*src[stride*8] - src[stride*10])>>4);
        src[stride*9] = clip_uint8((-src[stride*6] + 9*src[stride*8] + 9*src[stride*10] - src[stride*12])>>4);
        src++;
    }
}

static inline void linecpy(void *dest, const void *src, int lines, int stride) {
    if (stride > 0) {
        memcpy(dest, src, lines*stride);
    } else {
        memcpy((uint8_t*)dest+(lines-1)*stride, (const uint8_t*)src+(lines-1)*stride, -lines*stride);
    }
}

CPostProcess::CPostProcess()
{
	mCheckInit = false;
	mType = PP_TYPE_UNKNOWN;
	mWidth = 0;
	mHeight = 0;
}

CPostProcess::~CPostProcess()
{
}

bool CPostProcess::init(AW_PP_TYPE type, int width, int height)
{
	// pp_logd("pp width: %d, height: %d", width, height);
	
	mType = type;
	mWidth = width;
	mHeight = height;
	
	return true;
}

void CPostProcess::process(const uint8_t * prev[3],
							const uint8_t * src[3],
							const int srcStride[3],
							uint8_t * dst[3],
							const int dstStride[3])
{
	// pp_logd("process 0: %d, %d, %dx%d", srcStride[0], dstStride[0], mWidth, mHeight);
	if (mType == PP_TYPE_YADIF) {
		yadif_plane(dst[0], dstStride[0], prev[0], src[0], src[0], srcStride[0], mWidth, mHeight, 0, 1);
		yadif_plane(dst[1], dstStride[1], prev[1], src[1], src[1], srcStride[1], mWidth>>1, mHeight>>1, 0, 1);
		yadif_plane(dst[2], dstStride[2], prev[2], src[2], src[2], srcStride[2], mWidth>>1, mHeight>>1, 0, 1);
	} else {
		doProcess(src[0], srcStride[0], dst[0], dstStride[0], mWidth, mHeight);
		doProcess(src[1], srcStride[1], dst[1], dstStride[1], mWidth>>1, mHeight>>1);
		/* v plain in multithread, overflow occurs in some case(1920x1088), so copy last line manually */
		//doProcess(src[2], srcStride[2], dst[2], dstStride[2], mWidth>>1, mHeight>>1);
		doProcess(src[2], srcStride[2], dst[2], dstStride[2], mWidth>>1, (mHeight>>1)-1);
		memcpy(dst[2]+((mHeight>>1)-1)*dstStride[2], src[2]+((mHeight>>1)-1)*srcStride[2], mWidth>>1);
	}
}

void CPostProcess::doProcess(const uint8_t src[], 
							const int srcStride,
							uint8_t dst[], 
							const int dstStride,
							int width,
							int height)
{
	int x,y;
    int i;
    int copyAhead = (mType == PP_TYPE_CUBIC_INTERPOLATE) ? 16 : 14;		// cubic 16

	uint8_t * const tempSrc = mTempSrc;
	uint8_t * const tempDst = mTempDst;
	
	copyAhead-= 8;

	/* copy & deinterlace first row of blocks */
    y=-BLOCK_SIZE;
    {
        const uint8_t *srcBlock= &(src[y*srcStride]);
        uint8_t *dstBlock= tempDst + dstStride;

        // From this point on it is guaranteed that we can read and write 16 lines downward
        // finish 1 block before the next otherwise we might have a problem
        // with the L1 Cache of the P4 ... or only a few blocks at a time or something
        for(x=0; x<width; x+=BLOCK_SIZE){
			blockCopy(dstBlock + dstStride*8, dstStride,
                              srcBlock + srcStride*8, srcStride);
			duplicate(dstBlock + dstStride*8, dstStride);

			if (mType == PP_TYPE_LINEAR_BLEND)
				deInterlaceBlendLinear(dstBlock, dstStride, mDeintTemp + x);
			else if (mType == PP_TYPE_CUBIC_INTERPOLATE)
				deInterlaceInterpolateCubic(dstBlock, dstStride);
			
            dstBlock+=8;
            srcBlock+=8;
        }

		if(width==ABS(dstStride))
            linecpy(dst, tempDst + 9*dstStride, copyAhead, dstStride);
        else{
            int i;
            for(i=0; i<copyAhead; i++){
                memcpy(dst + i*dstStride, tempDst + (9+i)*dstStride, width);
            }
        }
	}

	for(y=0; y<height; y+=BLOCK_SIZE){
        //1% speedup if these are here instead of the inner loop
        const uint8_t *srcBlock= &(src[y*srcStride]);
        uint8_t *dstBlock= &(dst[y*dstStride]);

        /* can we mess with a 8x16 block from srcBlock/dstBlock downwards and 1 line upwards
           if not than use a temporary buffer */
        if(y+15 >= height){
            int i;
            /* copy from line (copyAhead) to (copyAhead+7) of src, these will be copied with
               blockcopy to dst later */
            linecpy(tempSrc + srcStride*copyAhead, srcBlock + srcStride*copyAhead,
                    MAX(height-y-copyAhead, 0), srcStride);

            /* duplicate last line of src to fill the void up to line (copyAhead+7) */
            for(i=MAX(height-y, 8); i<copyAhead+8; i++)
                    memcpy(tempSrc + srcStride*i, src + srcStride*(height-1), ABS(srcStride));

            /* copy up to (copyAhead+1) lines of dst (line -1 to (copyAhead-1))*/
			linecpy(tempDst, dstBlock - dstStride, MIN(height-y+1, copyAhead+1), dstStride);

            /* duplicate last line of dst to fill the void up to line (copyAhead) */
            for(i=height-y+1; i<=copyAhead; i++)
                    memcpy(tempDst + dstStride*i, dst + dstStride*(height-1), ABS(dstStride));

            dstBlock= tempDst + dstStride;
            srcBlock= tempSrc;
        }
		
        // From this point on it is guaranteed that we can read and write 16 lines downward
        // finish 1 block before the next otherwise we might have a problem
        // with the L1 Cache of the P4 ... or only a few blocks at a time or something
        for(x=0; x<width; x+=BLOCK_SIZE){
            const int stride= dstStride;

            blockCopy(dstBlock + dstStride*copyAhead, dstStride,
                              srcBlock + srcStride*copyAhead, srcStride);

			if (mType == PP_TYPE_LINEAR_BLEND)
				deInterlaceBlendLinear(dstBlock, dstStride, mDeintTemp + x);
			else if (mType == PP_TYPE_CUBIC_INTERPOLATE)
				deInterlaceInterpolateCubic(dstBlock, dstStride);

            dstBlock+=8;
            srcBlock+=8;
        }
	
        /* did we use a tmp buffer for the last lines*/
        if(y+15 >= height){
            uint8_t *dstBlock= &(dst[y*dstStride]);
            if(width==ABS(dstStride))
                linecpy(dstBlock, tempDst + dstStride, height-y, dstStride);
            else{
                int i;
                for(i=0; i<height-y; i++){
                    memcpy(dstBlock + i*dstStride, tempDst + (i+1)*dstStride, width);
                }
            }
        }
    }
}

