
//#define CONFIG_LOG_LEVEL    OPTION_LOG_LEVEL_DETAIL
#define LOG_TAG "layerControl_android_newDisplay"
#include "config.h"

#if CONFIG_OS_VERSION == OPTION_OS_VERSION_ANDROID_4_2
#include <gui/ISurfaceTexture.h>
#elif CONFIG_OS_VERSION >= OPTION_OS_VERSION_ANDROID_4_4
#include <gui/Surface.h>
#else
    #error "invalid configuration of os version."
#endif

#include <ui/Rect.h>
#include <ui/GraphicBufferMapper.h>
#include "layerControl_newDisplay.h"
#include "log.h"
#include "memoryAdapter.h"
#include <hardware/hwcomposer.h>

#include <hardware/hal_public.h>
#include <linux/ion.h>
#include <ion/ion.h>

#if (CONFIG_OS_VERSION == OPTION_OS_VERSION_ANDROID_5_0 && CONFIG_CHIP == OPTION_CHIP_1667)
#include "gralloc_priv.h"
#endif

#define GPU_BUFFER_NUM 32

#if CONFIG_CHIP == OPTION_CHIP_1639
#define PHY_OFFSET 0x20000000
#else
#define PHY_OFFSET 0x40000000
#endif


static VideoPicture* gLastPicture = NULL;

static int NUM_OF_PICTURES_KEEP_IN_LIST = 3;

typedef struct VPictureNode_t VPictureNode;
struct VPictureNode_t
{
    VideoPicture* pPicture;
    VideoPicture* pSecondPictureOf3D;
    VPictureNode* pNext;
    int           bUsed;
};

typedef struct GpuBufferInfoS
{
    ANativeWindowBuffer* pWindowBuf;

#if (CONFIG_OS_VERSION >= OPTION_OS_VERSION_ANDROID_5_0)
	ion_user_handle_t handle_ion;
#else
	struct ion_handle *handle_ion;
#endif

    char* pBufPhyAddr;
    char* pBufVirAddr;
    int   nUsedFlag;
    int   nDequeueFlag;
}GpuBufferInfoT;

typedef struct LayerCtrlContext
{
    ANativeWindow*       pNativeWindow;
    enum EPIXELFORMAT    eReceivePixelFormat;
    int                  nWidth;
    int                  nHeight;
    int                  nLeftOff;
    int                  nTopOff;
    int                  nDisplayWidth;
    int                  nDisplayHeight;
    PlayerCallback       callback;
    void*                pUserData;
    int                  bRenderToHardwareLayer;
    enum EPICTURE3DMODE  ePicture3DMode;
    enum EDISPLAY3DMODE  eDisplay3DMode;
    int                  bLayerInitialized;
    int                  bLayerShowed;
    int                  bDeinterlaceFlag;
    int                  nPreNativeWindowOutputType;
    int                  bProtectFlag;
    
    //* use when render to gpu.
    VideoPicture         bufferWrappers[32];
    int                  bBufferWrapperUsed[32];
    ANativeWindowBuffer* pWindowBufs[32];
    
    //* use when render derect to hardware layer.
    VPictureNode*        pPictureListHead;
    VPictureNode         picNodes[32];

    GpuBufferInfoT       mGpuBufferInfo[GPU_BUFFER_NUM];
    int                  nGpuBufferBusyNum;
    int                  nGpuBufferCount;
    int                  ionFd;
    int                  b4KAlignFlag;
    int                  bHadResetNativeWindowFlag;
    int                  bHoldLastPictureFlag;
    int                  bVideoWithTwoStreamFlag;
    int                  bIsSoftDecoderFlag;
	unsigned int         nUsage;
}LayerCtrlContext;

//* copy from ACodec.cpp
static int pushBlankBuffersToNativeWindow(LayerCtrlContext* lc) 
{
    logd("pushBlankBuffersToNativeWindow: pNativeWindow = %p",lc->pNativeWindow);

    if(lc->pNativeWindow == NULL)
    {
        logw(" the nativeWindow is null when call pushBlankBuffersToNativeWindow");
        return 0;
    }
    status_t err = NO_ERROR;
    ANativeWindowBuffer* pWindowBuffer = NULL;
    int numBufs = 0;
    int minUndequeuedBufs = 0;
    ANativeWindowBuffer **pArrBuffer = NULL;

    // We need to reconnect to the ANativeWindow as a CPU client to ensure that
    // no frames get dropped by SurfaceFlinger assuming that these are video
    // frames.
    err = native_window_api_disconnect(lc->pNativeWindow,NATIVE_WINDOW_API_MEDIA);
    if (err != NO_ERROR) {
        loge("error pushing blank frames: api_disconnect failed: %s (%d)",
                strerror(-err), -err);
        return err;
    }

    err = native_window_api_connect(lc->pNativeWindow,NATIVE_WINDOW_API_CPU);
    if (err != NO_ERROR) {
        loge("error pushing blank frames: api_connect failed: %s (%d)",
                strerror(-err), -err);
        return err;
    }

    err = native_window_set_buffers_geometry(lc->pNativeWindow, 1, 1,
            HAL_PIXEL_FORMAT_RGBX_8888);
    if (err != NO_ERROR) {
        loge("error pushing blank frames: set_buffers_geometry failed: %s (%d)",
                strerror(-err), -err);
        goto error;
    }

    err = native_window_set_scaling_mode(lc->pNativeWindow,
                NATIVE_WINDOW_SCALING_MODE_SCALE_TO_WINDOW);
    if (err != NO_ERROR) {
        loge("error pushing blank_frames: set_scaling_mode failed: %s (%d)",
              strerror(-err), -err);
        goto error;
    }

    err = native_window_set_usage(lc->pNativeWindow,
            GRALLOC_USAGE_SW_WRITE_OFTEN);
    if (err != NO_ERROR) {
        loge("error pushing blank frames: set_usage failed: %s (%d)",
                strerror(-err), -err);
        goto error;
    }

    err = lc->pNativeWindow->query(lc->pNativeWindow,
            NATIVE_WINDOW_MIN_UNDEQUEUED_BUFFERS, &minUndequeuedBufs);
    if (err != NO_ERROR) {
        loge("error pushing blank frames: MIN_UNDEQUEUED_BUFFERS query "
                "failed: %s (%d)", strerror(-err), -err);
        goto error;
    }

    numBufs = minUndequeuedBufs + 1;
    if (numBufs < 3)
        numBufs = 3;
    err = native_window_set_buffer_count(lc->pNativeWindow, numBufs);
    if (err != NO_ERROR) {
        loge("error pushing blank frames: set_buffer_count failed: %s (%d)",
                strerror(-err), -err);
        goto error;
    }

    // We  push numBufs + 1 buffers to ensure that we've drawn into the same
    // buffer twice.  This should guarantee that the buffer has been displayed
    // on the screen and then been replaced, so an previous video frames are
    // guaranteed NOT to be currently displayed.

    logd("numBufs=%d", numBufs);
    //* we just push numBufs.If push numBus+1,it will be problem in suspension window
    for (int i = 0; i < numBufs; i++) {
        int fenceFd = -1;
        err = native_window_dequeue_buffer_and_wait(lc->pNativeWindow, &pWindowBuffer);
        if (err != NO_ERROR) {
            loge("error pushing blank frames: dequeueBuffer failed: %s (%d)",
                    strerror(-err), -err);
            goto error;
        }

        sp<GraphicBuffer> buf(new GraphicBuffer(pWindowBuffer, false));

        // Fill the buffer with the a 1x1 checkerboard pattern ;)
        uint32_t* img = NULL;
        err = buf->lock(GRALLOC_USAGE_SW_WRITE_OFTEN, (void**)(&img));
        if (err != NO_ERROR) {
            loge("error pushing blank frames: lock failed: %s (%d)",
                    strerror(-err), -err);
            goto error;
        }

        *img = 0;

        err = buf->unlock();
        if (err != NO_ERROR) {
            loge("error pushing blank frames: unlock failed: %s (%d)",
                    strerror(-err), -err);
            goto error;
        }

        logd("queue...");
        err = lc->pNativeWindow->queueBuffer(lc->pNativeWindow,
                buf->getNativeBuffer(), -1);
        if (err != NO_ERROR) {
            loge("error pushing blank frames: queueBuffer failed: %s (%d)",
                    strerror(-err), -err);
            goto error;
        }

        pWindowBuffer = NULL;
    }

    pArrBuffer = (ANativeWindowBuffer **)malloc((numBufs)*sizeof(ANativeWindowBuffer*));
    for (int i = 0; i < numBufs-1; ++i) {
        err = native_window_dequeue_buffer_and_wait(lc->pNativeWindow, &pArrBuffer[i]);
        if (err != NO_ERROR) {
            loge("error pushing blank frames: dequeueBuffer failed: %s (%d)",
                        strerror(-err), -err);
            goto error;
        }
    }
    for (int i = 0; i < numBufs-1; ++i) {
        lc->pNativeWindow->cancelBuffer(lc->pNativeWindow, pArrBuffer[i], -1);
    }
    free(pArrBuffer);

error:

    if (err != NO_ERROR) {
        // Clean up after an error.
        if (pWindowBuffer != NULL) {
            lc->pNativeWindow->cancelBuffer(lc->pNativeWindow, pWindowBuffer, -1);
        }

        if (pArrBuffer) {
            free(pArrBuffer);
        }

        native_window_api_disconnect(lc->pNativeWindow,
                NATIVE_WINDOW_API_CPU);
        native_window_api_connect(lc->pNativeWindow,
                NATIVE_WINDOW_API_MEDIA);

        return err;
    } else {
        // Clean up after success.
        logd("disconnect...");
        err = native_window_api_disconnect(lc->pNativeWindow,
                NATIVE_WINDOW_API_CPU);
        if (err != NO_ERROR) {
            loge("error pushing blank frames: api_disconnect failed: %s (%d)",
                    strerror(-err), -err);
            return err;
        }

        err = native_window_api_connect(lc->pNativeWindow,
                NATIVE_WINDOW_API_MEDIA);
        if (err != NO_ERROR) {
            loge("error pushing blank frames: api_connect failed: %s (%d)",
                    strerror(-err), -err);
            return err;
        }

        return 0;
    }
}

static int SendThreeBlackFrameToGpu(LayerCtrlContext* lc)
{
    logd("SendThreeBlackFrameToGpu()");
    
    ANativeWindowBuffer* pWindowBuf;
    void*                pDataBuf;
    int                  i;
    int                  err;

    //* it just work on A80-box and H8         
#if((CONFIG_CHIP==OPTION_CHIP_1639 || CONFIG_CHIP==OPTION_CHIP_1673 || CONFIG_CHIP == OPTION_CHIP_1680) && (CONFIG_PRODUCT==OPTION_PRODUCT_TVBOX))    
    for(i = 0;i < NUM_OF_PICTURES_KEEP_IN_LIST;i++)
    {
        err = lc->pNativeWindow->dequeueBuffer_DEPRECATED(lc->pNativeWindow, &pWindowBuf);
        if(err != 0)
        {
            logw("dequeue buffer fail, return value from dequeueBuffer_DEPRECATED() method is %d.", err);
            return -1;
        }
        lc->pNativeWindow->lockBuffer_DEPRECATED(lc->pNativeWindow, pWindowBuf);

        //* lock the data buffer.
        {
            GraphicBufferMapper& mapper = GraphicBufferMapper::get();
            Rect bounds(lc->nWidth, lc->nHeight);
            mapper.lock(pWindowBuf->handle, /*lc->nUsage*/GRALLOC_USAGE_SW_WRITE_OFTEN, bounds, &pDataBuf);
			logd("mapper %p", pDataBuf);
        }

        if (pDataBuf) {
            memset((char*)pDataBuf,0x10,(pWindowBuf->height * pWindowBuf->stride));
            memset((char*)pDataBuf + pWindowBuf->height * pWindowBuf->stride,0x80,(pWindowBuf->height * pWindowBuf->stride)/2);
        }
        
        int nBufAddr[7] = {0};
        //nBufAddr[6] = HAL_PIXEL_FORMAT_AW_NV12;
        nBufAddr[6] = HAL_PIXEL_FORMAT_AW_FORCE_GPU;
        lc->pNativeWindow->perform(lc->pNativeWindow, NATIVE_WINDOW_SET_VIDEO_BUFFERS_INFO,	nBufAddr[0], nBufAddr[1],
        nBufAddr[2], nBufAddr[3], nBufAddr[4], nBufAddr[5], nBufAddr[6]);
        
        //* unlock the buffer.
        {
            GraphicBufferMapper& mapper = GraphicBufferMapper::get();
            mapper.unlock(pWindowBuf->handle);
        }

        lc->pNativeWindow->queueBuffer_DEPRECATED(lc->pNativeWindow, pWindowBuf);

    }
#endif
    return 0;
}

// dequeue all nativewindow buffer from BufferQueue, then cancle all.
// this can make sure all buffer queued by player to render by surfaceflinger.
static int MakeSureFramesToShow(LayerCtrlContext* lc)
{
	logd("MakeSureFramesToShow()");
    
    ANativeWindowBuffer* pWindowBuf[32];
    void*                pDataBuf;
    int                  i;
    int                  err;
	int					 bufCnt = lc->nGpuBufferCount;
     
    for(i = 0;i < bufCnt -1; i++)
    {
        err = lc->pNativeWindow->dequeueBuffer_DEPRECATED(lc->pNativeWindow, &pWindowBuf[i]);
        if(err != 0)
        {
            logw("dequeue buffer fail, return value from dequeueBuffer_DEPRECATED() method is %d.", err);
            break;
        }
        
    	logv("dequeue i = %d, handle: 0x%x", i, pWindowBuf[i]->handle);

    }

    for(i--; i >= 0; i--)
    {        
    	logv("cancel i = %d, handle: 0x%x", i, pWindowBuf[i]->handle);
        lc->pNativeWindow->cancelBuffer(lc->pNativeWindow, pWindowBuf[i], -1);
    }
	
    return 0;
}


static int SetLayerParam(LayerCtrlContext* lc)
{
    logd("SetLayerParam: PixelFormat = %d, nW = %d, nH = %d, leftoff = %d, topoff = %d \
          dispW = %d, dispH = %d, buffercount = %d",
          lc->eReceivePixelFormat,
          lc->nWidth,
          lc->nHeight,
          lc->nLeftOff,
          lc->nTopOff,
          lc->nDisplayWidth,
          lc->nDisplayHeight,
          lc->nGpuBufferCount);
    
    int          pixelFormat;
    unsigned int nGpuBufWidth;
    unsigned int nGpuBufHeight;
    Rect         crop;

    logd("lc->bProtectFlag = %d, bIsSoftDecoderFlag = %d",
          lc->bProtectFlag,
          lc->bIsSoftDecoderFlag);
    if(lc->bProtectFlag == 1)
    {
        // Verify that the ANativeWindow sends images directly to
        // SurfaceFlinger.
        int err = -1;
        int queuesToNativeWindow = 0;
        err = lc->pNativeWindow->query(
                lc->pNativeWindow, NATIVE_WINDOW_QUEUES_TO_WINDOW_COMPOSER,
                &queuesToNativeWindow);
        if (err != 0) {
            loge("error authenticating native window: %d", err);
            return err;
        }
        if (queuesToNativeWindow != 1) {
            loge("native window could not be authenticated");
            return PERMISSION_DENIED;
        }
        logd("set usage to GRALLOC_USAGE_PROTECTED");
        lc->nUsage |= GRALLOC_USAGE_PROTECTED;
    }

	#if(USE_NEW_DISPLAY == 1)
    if(lc->bIsSoftDecoderFlag == 1)
    {
        lc->nUsage |= GRALLOC_USAGE_SW_WRITE_OFTEN;
		if (GPU_TYPE_MALI == 1)
            lc->nUsage |= GRALLOC_USAGE_PRIVATE_3;
    }
	#endif
    
    switch(lc->eReceivePixelFormat)
    {
        case PIXEL_FORMAT_YV12:             //* why YV12 use this pixel format.
            pixelFormat = HAL_PIXEL_FORMAT_YV12;
            break;
        case PIXEL_FORMAT_NV21:
            pixelFormat = HAL_PIXEL_FORMAT_YCrCb_420_SP;
            break;
        case PIXEL_FORMAT_NV12: //* display system do not support NV12.
        default:
        {
            loge("unsupported pixel format.");
            return -1;
            break;
        }
    }

	if (lc->bProtectFlag) {
	    lc->nUsage |=  GRALLOC_USAGE_SW_READ_NEVER     |
              // GRALLOC_USAGE_SW_WRITE_OFTEN    | 
              GRALLOC_USAGE_HW_TEXTURE        | 
              GRALLOC_USAGE_EXTERNAL_DISP;
	} else {
	    lc->nUsage |= // GRALLOC_USAGE_SW_READ_NEVER     |
              GRALLOC_USAGE_SW_WRITE_OFTEN    |
              GRALLOC_USAGE_SW_READ_OFTEN     |
              GRALLOC_USAGE_HW_TEXTURE        |
              GRALLOC_USAGE_EXTERNAL_DISP;
	}
    
    native_window_set_usage(lc->pNativeWindow,lc->nUsage);

    native_window_set_scaling_mode(lc->pNativeWindow, NATIVE_WINDOW_SCALING_MODE_SCALE_TO_WINDOW);

    #if 0
    nGpuBufWidth  = (lc->nWidth + 15) & ~15;
    nGpuBufHeight = (lc->nHeight + 15) & ~15;
    
    //A10's GPU has a bug, we can avoid it
    if(nGpuBufHeight%8 != 0)
    {
        logw("original picture align_width[%d], height[%d] mod8 = %d", nGpuBufWidth, nGpuBufHeight, nGpuBufHeight%8);
        if((nGpuBufWidth*nGpuBufHeight)%256 != 0)
        {
            logw("original picture align_width[%d]*height[%d] mod 1024 = %d", 
                   nGpuBufWidth, nGpuBufHeight, (nGpuBufWidth*nGpuBufHeight)%1024);
            nGpuBufHeight = (nGpuBufHeight+7)&~7;
            logw("change picture height to [%d] when render to gpu", nGpuBufHeight);
        }
    }
    #endif
    
    nGpuBufWidth  = lc->nWidth;  //* restore nGpuBufWidth to mWidth;
    nGpuBufHeight = lc->nHeight;

    //* We should double the height if the video with two stream,
    //* so the nativeWindow will malloc double buffer
    if(lc->bVideoWithTwoStreamFlag == 1)
    {
        nGpuBufHeight = 2*nGpuBufHeight;
    }
    
    native_window_set_buffers_geometry(lc->pNativeWindow, nGpuBufWidth, nGpuBufHeight, pixelFormat);

    crop.left   = lc->nLeftOff;
    crop.top    = lc->nTopOff;
    crop.right  = lc->nLeftOff + lc->nDisplayWidth;
    crop.bottom = lc->nTopOff + lc->nDisplayHeight;
    lc->pNativeWindow->perform(lc->pNativeWindow, NATIVE_WINDOW_SET_CROP, &crop);

    logd("nGpuBufferCount = %d",lc->nGpuBufferCount);
    //* set the buffer count
    if(lc->nGpuBufferCount != 0)
        native_window_set_buffer_count(lc->pNativeWindow,lc->nGpuBufferCount);
    else
    {
        loge("error: the nativeWindow buffer Count is 0!");
        return -1;
    }

    
    return 0;
}

LayerCtrl* LayerInit(void* pNativeWindow, int bProtectedFlag)
{
    logv("LayerInit, pNativeWindow = %p",pNativeWindow);
    
    LayerCtrlContext* lc;
    
    lc = (LayerCtrlContext*)malloc(sizeof(LayerCtrlContext));
    if(lc == NULL)
    {
        loge("malloc memory fail.");
        return NULL;
    }
    memset(lc, 0, sizeof(LayerCtrlContext));

    lc->ionFd = -1;
    lc->ionFd = ion_open();

    logd("ion open fd = %d",lc->ionFd);
    if(lc->ionFd < -1)
    {
        loge("ion open fail ! ");
        return NULL;
    }

    lc->bProtectFlag  = bProtectedFlag;
    lc->pNativeWindow = (ANativeWindow*)pNativeWindow;
    return (LayerCtrl*)lc;
}


void LayerRelease(LayerCtrl* l, int bKeepPictureOnScreen)
{
    LayerCtrlContext* lc;
    VPictureNode*     nodePtr;
    int i;
    int ret;
    VideoPicture mPicBufInfo;

	CEDARX_UNUSE(bKeepPictureOnScreen);
	
    lc = (LayerCtrlContext*)l;

    memset(&mPicBufInfo, 0, sizeof(VideoPicture));

    logv("LayerRelease, ionFd = %d",lc->ionFd);

	if(lc->bProtectFlag == 0)
	{
		if(lc->bHoldLastPictureFlag == false/* && lc->bFirstFrameIsShowed == 1*/)
		{
			SendThreeBlackFrameToGpu(lc);
		}
    }

	MakeSureFramesToShow(lc);

    for(i = 0; i < lc->nGpuBufferCount; i++)
    {
        //* we should queue buffer which had dequeued to gpu
        if(lc->mGpuBufferInfo[i].nDequeueFlag == 1)
        {
            logd("queueBuffer when release");
            mPicBufInfo.phyYBufAddr = (unsigned int)lc->mGpuBufferInfo[i].pBufPhyAddr;
            mPicBufInfo.nBufId      = i;
            LayerQueueBuffer(l, &mPicBufInfo, 0);
        }

#if (CONFIG_OS_VERSION == OPTION_OS_VERSION_ANDROID_5_0)

        if(lc->mGpuBufferInfo[i].handle_ion != 0)
        {
            logd("ion_free: handle_ion[%d] = %p",i,lc->mGpuBufferInfo[i].handle_ion);
            ion_free(lc->ionFd,lc->mGpuBufferInfo[i].handle_ion);
        }
#else
		if(lc->mGpuBufferInfo[i].handle_ion != NULL)
		{
			logd("ion_free: handle_ion[%d] = %p",i,lc->mGpuBufferInfo[i].handle_ion);
			ion_free(lc->ionFd,lc->mGpuBufferInfo[i].handle_ion);
		}
#endif
    }

    if(lc->ionFd > 0)
    {
        ion_close(lc->ionFd);
    }

    if(lc->bProtectFlag == 1 /*|| lc->bHoldLastPictureFlag == 0*/)
    {
        ret = pushBlankBuffersToNativeWindow(lc);
        if(ret != 0)
        {
            loge("pushBlankBuffersToNativeWindow appear error!: ret = %d",ret);
        }
    }
    
    free(lc);    
}

void LayerResetNativeWindow(LayerCtrl* l,void* pNativeWindow)
{
    logd("LayerResetNativeWindow : %p ",pNativeWindow);
    
    LayerCtrlContext* lc;
    VideoPicture mPicBufInfo;

    lc = (LayerCtrlContext*)l; 

    lc->bHadResetNativeWindowFlag = 1;
    lc->nGpuBufferBusyNum         = 0;
    memset(&mPicBufInfo, 0, sizeof(VideoPicture));
    
    //* free the buffer which had been returned
    int i;
    for(i = 0; i < GPU_BUFFER_NUM; i++)
    {
        //* we should queue buffer which had dequeued to gpu
        if(lc->mGpuBufferInfo[i].nDequeueFlag == 1)
        {
            mPicBufInfo.phyYBufAddr = (unsigned int)lc->mGpuBufferInfo[i].pBufPhyAddr;
            mPicBufInfo.nBufId      = i;
            LayerQueueBuffer(l, &mPicBufInfo, 0);
            lc->mGpuBufferInfo[i].nDequeueFlag = 0;
        }


#if (CONFIG_OS_VERSION >= OPTION_OS_VERSION_ANDROID_5_0)        
        if(lc->mGpuBufferInfo[i].nUsedFlag == 0
           && lc->mGpuBufferInfo[i].handle_ion != 0)
        {
            if(lc->ionFd > 0)
            {
                logi("ion_free when LayerResetNativeWindow");
                ion_free(lc->ionFd,lc->mGpuBufferInfo[i].handle_ion);
                lc->mGpuBufferInfo[i].handle_ion = 0;
            }
            else
            {
                loge("the ionFd is invalid when call ion_free(), ionFd = %d",lc->ionFd);
                abort();
            }
        }
#else
		if(lc->mGpuBufferInfo[i].nUsedFlag == 0
		   && lc->mGpuBufferInfo[i].handle_ion != NULL)
		{
			if(lc->ionFd > 0)
			{
				logi("ion_free when LayerResetNativeWindow");
				ion_free(lc->ionFd,lc->mGpuBufferInfo[i].handle_ion);
				lc->mGpuBufferInfo[i].handle_ion = NULL;
			}
			else
			{
				loge("the ionFd is invalid when call ion_free(), ionFd = %d",lc->ionFd);
				abort();
			}
		}
#endif

    }




    memset(&lc->mGpuBufferInfo,0,sizeof(GpuBufferInfoT)*GPU_BUFFER_NUM);

    lc->pNativeWindow = (ANativeWindow*)pNativeWindow;

    if(lc->pNativeWindow != NULL)
        lc->bLayerInitialized = 0;
    
    return ;
}

int LayerSetExpectPixelFormat(LayerCtrl* l, enum EPIXELFORMAT ePixelFormat)
{
    LayerCtrlContext* lc;
    
    lc = (LayerCtrlContext*)l;

    logv("Layer set expected pixel format, format = %d", (int)ePixelFormat);
    
    if(ePixelFormat == PIXEL_FORMAT_NV12 || 
       ePixelFormat == PIXEL_FORMAT_NV21 ||
       ePixelFormat == PIXEL_FORMAT_YV12)           //* add new pixel formats supported by gpu here.
    {
        lc->eReceivePixelFormat = ePixelFormat;
    }
    else
    {
        logv("receive pixel format is %d, not match.", lc->eReceivePixelFormat);
        return -1;
    }

    //* on A83-pad and A83-box , the address should 4k align when format is NV21
#if(CONFIG_CHIP == OPTION_CHIP_1673)
    if(lc->eReceivePixelFormat == PIXEL_FORMAT_NV21)
        lc->b4KAlignFlag = 1;
#endif
    
    return 0;
}


int LayerSetPictureSize(LayerCtrl* l, int nWidth, int nHeight)
{
    LayerCtrlContext* lc;
    
    lc = (LayerCtrlContext*)l;

    logv("Layer set picture size, width = %d, height = %d", nWidth, nHeight);
    
    lc->nWidth         = nWidth;
    lc->nHeight        = nHeight;
    lc->nDisplayWidth  = nWidth;
    lc->nDisplayHeight = nHeight;
    lc->nLeftOff       = 0;
    lc->nTopOff        = 0;
    lc->bLayerInitialized = 0;
    
    return 0;
}


int LayerSetDisplayRegion(LayerCtrl* l, int nLeftOff, int nTopOff, int nDisplayWidth, int nDisplayHeight)
{
    LayerCtrlContext* lc;
    
    lc = (LayerCtrlContext*)l;

    logv("Layer set display region, leftOffset = %d, topOffset = %d, displayWidth = %d, displayHeight = %d",
        nLeftOff, nTopOff, nDisplayWidth, nDisplayHeight);

	int scaler = (lc->bVideoWithTwoStreamFlag == 1) ? 2 : 1;
	int displayHeight = (lc->bVideoWithTwoStreamFlag == 1) ? lc->nDisplayHeight : nDisplayHeight;
    
    if(nDisplayWidth != 0 && nDisplayHeight != 0)
    {
        lc->nDisplayWidth     = nDisplayWidth;
        lc->nDisplayHeight    = nDisplayHeight;
        lc->nLeftOff          = nLeftOff;
        lc->nTopOff           = nTopOff;

        if(lc->bLayerInitialized == 1)
        {
            Rect         crop;
            crop.left   = lc->nLeftOff;
            crop.top    = lc->nTopOff;
            crop.right  = lc->nLeftOff + lc->nDisplayWidth;
            crop.bottom = lc->nTopOff + displayHeight * scaler;
            lc->pNativeWindow->perform(lc->pNativeWindow, NATIVE_WINDOW_SET_CROP, &crop);
        }
        
        return 0;
    }
    else
        return -1;
}

int LayerSetBufferCount(LayerCtrl* l, int nBufferCount)
{
    LayerCtrlContext* lc;

    lc = (LayerCtrlContext*)l;

    logv("LayerSetBufferCount: count = %d",nBufferCount);
    
    lc->nGpuBufferCount = nBufferCount + NUM_OF_PICTURES_KEEP_IN_LIST;

    if(lc->nGpuBufferCount > GPU_BUFFER_NUM)
        lc->nGpuBufferCount = GPU_BUFFER_NUM;
    return 0;
}

int LayerSetVideoWithTwoStreamFlag(LayerCtrl* l, int bVideoWithTwoStreamFlag)
{
    LayerCtrlContext* lc;

    lc = (LayerCtrlContext*)l;

    logv("LayerSetIsTwoVideoStreamFlag, flag = %d",bVideoWithTwoStreamFlag);

    lc->bVideoWithTwoStreamFlag = bVideoWithTwoStreamFlag;

    return 0;
}

int LayerSetIsSoftDecoderFlag(LayerCtrl* l, int bIsSoftDecoderFlag)
{
    LayerCtrlContext* lc;

    lc = (LayerCtrlContext*)l;

    logv("LayerSetIsTwoVideoStreamFlag, flag = %d",bVideoWithTwoStreamFlag);

    lc->bIsSoftDecoderFlag = bIsSoftDecoderFlag;

    return 0;
}

int LayerDequeueBuffer(LayerCtrl* l,VideoPicture* pPicBufInfo)
{
    LayerCtrlContext* lc;
    
    lc = (LayerCtrlContext*)l;

    logv("LayerDequeueBuffer");
    
    pPicBufInfo->phyYBufAddr = -1;
    pPicBufInfo->phyCBufAddr = -1;
    pPicBufInfo->nBufId      = -1;

    //* dequeue a buffer from the native window object, set it to a picture buffer wrapper.
    ANativeWindowBuffer* pWindowBuf = NULL;
	
#if (CONFIG_OS_VERSION >= OPTION_OS_VERSION_ANDROID_5_0)
	ion_user_handle_t handle_ion = 0;
#else
	struct ion_handle *handle_ion  = NULL;
#endif

    void*   pDataBuf    = NULL;
    int     nPhyaddress = -1;
    int     err = -1;
    int     i   = 0;
    int     nNeedCancelFlag = 0;
    int     nCancelNum = 0;
    int     nCancelIndex[GPU_BUFFER_NUM] = {-1};

    if(lc->pNativeWindow == NULL)
    {
        logw("pNativeWindow is null when dequeue buffer");
        pPicBufInfo->pData0      = NULL;
        pPicBufInfo->pData1      = NULL;
        pPicBufInfo->phyYBufAddr = -1;
        pPicBufInfo->phyCBufAddr = -1;
        pPicBufInfo->nBufId      = -1;
        pPicBufInfo->pPrivate    = NULL;

        return -1;
    }
        
    if(lc->bLayerInitialized == 0)
    {
        if(SetLayerParam(lc) != 0)
        {
            loge("can not initialize layer.");
            return -1;
        }
        
        lc->bLayerInitialized = 1;
        
    }

    logv("nGpuBufferBusyNum = %d, nGpuBufferCount = %d",
          lc->nGpuBufferBusyNum,
          (lc->nGpuBufferCount - NUM_OF_PICTURES_KEEP_IN_LIST));
    if(lc->nGpuBufferBusyNum >= (lc->nGpuBufferCount - NUM_OF_PICTURES_KEEP_IN_LIST))
    {
        logv("no gpu buffer to use: BufferCount = %d, busyNum = %d",
              lc->nGpuBufferCount,
              lc->nGpuBufferBusyNum);
        //*pBuf   = NULL;
        //*nBufId = -1;
        pPicBufInfo->phyYBufAddr = -1;
        pPicBufInfo->phyCBufAddr = -1;
        pPicBufInfo->nBufId      = -1;
        return -1;
    }
    
dequeue_buffer:
    
    //* dequeue a buffer from the nativeWindow object.
    err = lc->pNativeWindow->dequeueBuffer_DEPRECATED(lc->pNativeWindow, &pWindowBuf);
    if(err != 0)
    {
        logw("dequeue buffer fail, return value from dequeueBuffer_DEPRECATED() method is %d.", err);
        return -1;
    }

    for(i = 0; i < lc->nGpuBufferCount; i++)
    {
        if(lc->mGpuBufferInfo[i].nUsedFlag == 1
           && lc->mGpuBufferInfo[i].pWindowBuf == pWindowBuf)
        {
            nNeedCancelFlag = 1;
            nCancelIndex[nCancelNum] = i;
            nCancelNum++;
            logv("the buffer have not return ,dequeue agagin! : %p",pWindowBuf);
            goto dequeue_buffer;
        }
    }

    if(nNeedCancelFlag == 1)
    {
        for(i = 0;i<nCancelNum;i++)
        {
            int nIndex = nCancelIndex[i];
            ANativeWindowBuffer* pTmpWindowBuf = lc->mGpuBufferInfo[nIndex].pWindowBuf;
            lc->pNativeWindow->cancelBuffer_DEPRECATED(lc->pNativeWindow, pTmpWindowBuf);
        }
        nCancelNum = 0;
        nNeedCancelFlag = 0;
    }
    
    lc->pNativeWindow->lockBuffer_DEPRECATED(lc->pNativeWindow, pWindowBuf);

    //* lock the data buffer.
    {
        GraphicBufferMapper& mapper = GraphicBufferMapper::get();
        Rect bounds(lc->nWidth, lc->nHeight);
        mapper.lock(pWindowBuf->handle, lc->nUsage, bounds, &pDataBuf);
    }

    for(i = 0; i < lc->nGpuBufferCount; i++)
    {
        if(lc->mGpuBufferInfo[i].nUsedFlag == 0
           && lc->mGpuBufferInfo[i].pWindowBuf == NULL)
        {
//for mali GPU
#if(GPU_TYPE_MALI == 1)
			private_handle_t* hnd = (private_handle_t *)(pWindowBuf->handle);
#else 
            IMG_native_handle_t* hnd = (IMG_native_handle_t*)(pWindowBuf->handle);
#endif

            if(hnd != NULL)
            {
#if(GPU_TYPE_MALI == 1)
				ion_import(lc->ionFd, hnd->share_fd, &handle_ion);
#else
				ion_import(lc->ionFd, hnd->fd[0], &handle_ion);
#endif
            }
            else
            {
                logd("the hnd is wrong : hnd = %p",hnd);
                return -1;
            }

            //* we should not get the phyaddr if it is software decoder
            if(lc->bIsSoftDecoderFlag == 0)
            {
                if(lc->ionFd > 0)
                    nPhyaddress = ion_getphyadr(lc->ionFd, handle_ion);
                else
                {
                    logd("the ion fd is wrong : fd = %d",lc->ionFd);
                    return -1;
                }

                nPhyaddress -= PHY_OFFSET;
            }

            lc->mGpuBufferInfo[i].pWindowBuf   = pWindowBuf;
            lc->mGpuBufferInfo[i].handle_ion   = handle_ion;
            lc->mGpuBufferInfo[i].pBufVirAddr  = (char*)pDataBuf;
            lc->mGpuBufferInfo[i].pBufPhyAddr  = (char*)nPhyaddress;
            lc->mGpuBufferInfo[i].nUsedFlag    = 1;
            lc->mGpuBufferInfo[i].nDequeueFlag = 1;
            break;
        }
        else if(lc->mGpuBufferInfo[i].nUsedFlag == 0
                && lc->mGpuBufferInfo[i].pWindowBuf == pWindowBuf)
        {
            lc->mGpuBufferInfo[i].nUsedFlag    = 1;
            lc->mGpuBufferInfo[i].nDequeueFlag = 1;
            break;
        }
    }
    
    if(i == lc->nGpuBufferCount)
    {
        loge("not enouth gpu buffer , should not run here");
        abort();
    }

    //* set the buffer address
    pPicBufInfo->pData0      = lc->mGpuBufferInfo[i].pBufVirAddr;
    pPicBufInfo->pData1      = pPicBufInfo->pData0 + (pWindowBuf->height * pWindowBuf->stride);
    pPicBufInfo->pData2      = pPicBufInfo->pData1 + (pWindowBuf->height * pWindowBuf->stride)/4;
    pPicBufInfo->phyYBufAddr = (unsigned int)lc->mGpuBufferInfo[i].pBufPhyAddr;
    pPicBufInfo->phyCBufAddr = pPicBufInfo->phyYBufAddr + (pWindowBuf->height * pWindowBuf->stride);
    pPicBufInfo->nBufId      = i;
    pPicBufInfo->pPrivate    = (void*)lc->mGpuBufferInfo[i].handle_ion;
    pPicBufInfo->ePixelFormat= lc->eReceivePixelFormat;
    pPicBufInfo->nWidth      = pWindowBuf->stride;
    pPicBufInfo->nHeight     = pWindowBuf->height;
    pPicBufInfo->nLineStride = pWindowBuf->stride;
    
    if(lc->b4KAlignFlag == 1)
    {
        unsigned int tmpAddr = (unsigned int)pPicBufInfo->pData1;
        tmpAddr     = (tmpAddr + 4095) & ~4095;
        
        pPicBufInfo->pData1      = (char *)tmpAddr;
        pPicBufInfo->phyCBufAddr = (pPicBufInfo->phyCBufAddr + 4095) & ~4095;
    }

    lc->nGpuBufferBusyNum++;
    
    logv("windH = %d, windW = %d, phyYBufAddr = %x, phyCBufAddr = %x, handle_ion = %p, ionFd = %d",
          pWindowBuf->height,
          pWindowBuf->stride,
          pPicBufInfo->phyYBufAddr,
          pPicBufInfo->phyCBufAddr,
          handle_ion,
          lc->ionFd);
    
    return 0;
}
int LayerQueueBuffer(LayerCtrl* l,VideoPicture* pPicBufInfo, int bValid)
{

    ANativeWindowBuffer* pWindowBuf = NULL;
    LayerCtrlContext* lc  = NULL;
    int               i   = 0;
    char*             pBuf   = NULL;
    int               nBufId = -1;
    
    lc = (LayerCtrlContext*)l;

    if(lc->bLayerInitialized == 0)
    {
        if(SetLayerParam(lc) != 0)
        {
            loge("can not initialize layer.");
            return -1;
        }
        
        lc->bLayerInitialized = 1;
        
    }

    pBuf   = (char*)pPicBufInfo->phyYBufAddr;
    nBufId = pPicBufInfo->nBufId;
    
    logv("LayerQueueBuffer: pBuf = %p, id = %d, bValid = %d",pBuf,nBufId, bValid);
    
    if(pBuf != lc->mGpuBufferInfo[nBufId].pBufVirAddr 
       && pBuf != lc->mGpuBufferInfo[nBufId].pBufPhyAddr)
    {
        if(lc->bHadResetNativeWindowFlag == 1)
        {
            //* We do nothing here when had reset nativeWindow,
            //* because we had queued the buffer in the call LayerResetNativeWindow.
            logw("do nothing when queue buffer if had reset nativeWindow!");
            return 0;
        }
        else
        {
            loge("error: the queue buffer is not exist, %p, %p, %p",
                  lc->mGpuBufferInfo[nBufId].pBufVirAddr,
                  lc->mGpuBufferInfo[nBufId].pBufPhyAddr,
                  pBuf);
            abort();
            return -1;
        }
    }

    pWindowBuf = lc->mGpuBufferInfo[nBufId].pWindowBuf;
    
    //* unlock the buffer.
    {
        GraphicBufferMapper& mapper = GraphicBufferMapper::get();
        mapper.unlock(pWindowBuf->handle);
    }

    if(bValid == 1)
        lc->pNativeWindow->queueBuffer_DEPRECATED(lc->pNativeWindow, pWindowBuf);
    else
        lc->pNativeWindow->cancelBuffer_DEPRECATED(lc->pNativeWindow, pWindowBuf);

    lc->mGpuBufferInfo[nBufId].nDequeueFlag = 0;
    
    logv("******LayerQueueBuffer finish!");
    return 0;
}

int LayerReturnBuffer(LayerCtrl* l,VideoPicture* pPicBufInfo)
{
    LayerCtrlContext* lc;
    char*             pBuf       = NULL;
    int               nBufId     = -1;
    void*             pIonHandle = NULL;
    
    lc = (LayerCtrlContext*)l;

    pBuf       = (char*)pPicBufInfo->phyYBufAddr;
    nBufId     = pPicBufInfo->nBufId;
    pIonHandle = pPicBufInfo->pPrivate;

    if(pBuf != lc->mGpuBufferInfo[nBufId].pBufVirAddr
       && pBuf != lc->mGpuBufferInfo[nBufId].pBufPhyAddr)
    {
        if(lc->bHadResetNativeWindowFlag == 1)
        {

#if (CONFIG_OS_VERSION >= OPTION_OS_VERSION_ANDROID_5_0)
			//* We should ion_free the buffer if had reset nativeWindow right now.
			logw("ion_free when return buffer!, handle_ion = %p",(ion_user_handle_t)pIonHandle);
			ion_free(lc->ionFd,(ion_user_handle_t)pIonHandle);
#else
			//* We should ion_free the buffer if had reset nativeWindow right now.
			logw("ion_free when return buffer!, handle_ion = %p",(ion_handle*)pIonHandle);
			ion_free(lc->ionFd,(ion_handle*)pIonHandle);
#endif
            return 0;
        }
        else
        {
            loge("error: the queue buffer is not exist, %p, %p, %p",
                  lc->mGpuBufferInfo[nBufId].pBufVirAddr,
                  lc->mGpuBufferInfo[nBufId].pBufPhyAddr,
                  pBuf);
            abort();
            return -1;
        }
    }
                                             
    lc->mGpuBufferInfo[nBufId].nUsedFlag  = 0;
    lc->nGpuBufferBusyNum--;

    return 0;
}

int LayerCtrlShowVideo(LayerCtrl* l)
{
    LayerCtrlContext* lc;
    int               i;

    lc = (LayerCtrlContext*)l;
    
    logv("LayerCtrlShowVideo, current show flag = %d", lc->bLayerShowed);
    
    if(lc->bLayerShowed == 0)
    {
        if(lc->pNativeWindow != NULL)
        {
        	lc->bLayerShowed = 1;
            lc->pNativeWindow->perform(lc->pNativeWindow,
                                       NATIVE_WINDOW_SETPARAMETER,
                                       HWC_LAYER_SHOW,
                                       1);
        }
        else
        {
            logw("the nativeWindow is null when call LayerCtrlShowVideo()");
            return -1;
        }
    }
    return 0;
}

int LayerCtrlHideVideo(LayerCtrl* l)
{
    LayerCtrlContext* lc;
    int               i;

    lc = (LayerCtrlContext*)l;
    
    logv("LayerCtrlHideVideo, current show flag = %d", lc->bLayerShowed);
    
    if(lc->bLayerShowed == 1)
    {
        if(lc->pNativeWindow != NULL)
        {
    	lc->bLayerShowed = 0;
        lc->pNativeWindow->perform(lc->pNativeWindow,
                                       NATIVE_WINDOW_SETPARAMETER,
                                       HWC_LAYER_SHOW,
                                       0);
        }
        else
        {
            logw("the nativeWindow is null when call LayerCtrlHideVideo()");
            return -1;
        }
    }
    return 0;
}

int LayerCtrlIsVideoShow(LayerCtrl* l)
{
    LayerCtrlContext* lc;

    lc = (LayerCtrlContext*)l;

    logv("LayerCtrlIsVideoShow : bLayerShowed = %d",lc->bLayerShowed);
    
    return lc->bLayerShowed;
}

int LayerCtrlHoldLastPicture(LayerCtrl* l, int bHold)
{
    logv("LayerCtrlHoldLastPicture, bHold = %d", bHold);
    //*TODO
    LayerCtrlContext* lc;

    lc = (LayerCtrlContext*)l;

    lc->bHoldLastPictureFlag = bHold;
    
    return 0;
}

