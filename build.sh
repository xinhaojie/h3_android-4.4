#!/bin/bash

function usage()
{
    echo -e "\033[1;32mUsage: `basename $0` -b board[${H3_ANDROID_BOARD}]\033[0m"
    exit 1
}

# function pt_error()
# {
#     echo -e "\033[1;31mERROR: $*\033[0m"
# }

# function pt_warn()
# {
#     echo -e "\033[1;31mWARN: $*\033[0m"
# }

function pt_info()
{
    echo -e "\033[1;32mINFO: $*\033[0m"
}

function execute_cmd() 
{
    pt_info $@
    eval $@ || exit $?
}

function parse_arg()
{
    if [ $# -ne 2 ]; then
        usage
    fi
    OLD_IFS=${IFS}
    IFS="|"
    while getopts "b:" opt
    do
        case $opt in
            b )
                BOARD_NAME=$OPTARG
                for TMP in ${H3_ANDROID_BOARD}
                do
                    if [ ${BOARD_NAME} = ${TMP} ];then
                        FOUND=1
                        break
                    else
                        FOUND=0
                    fi
                done
                if [[ ${FOUND} -ne 1 || ! -e ${BOARDCONFIG_DIR}/BoardConfig_${BOARD_NAME}.mk ]]; then
                    echo "unsupported board"
                    usage
                fi
                ;;
            ? )
                usage
                ;;
            esac
    done
    IFS=${OLD_IFS}
}

BOARD_NAME=none
BOARDCONFIG_DIR=device/softwinner/nanopi-h3/BoardConfig
H3_ANDROID_BOARD="nanopi-m1|nanopi-m1-plus"
parse_arg $@

pt_info "preparing boardconfig.mk"
execute_cmd "cp -v ${BOARDCONFIG_DIR}/BoardConfig_${BOARD_NAME}.mk ${BOARDCONFIG_DIR}/../BoardConfig.mk"
export PATH=/usr/lib/jvm/jdk1.6.0_45/bin:$PATH
source ./build/envsetup.sh
execute_cmd "lunch nanopi_h3-eng"
execute_cmd "extract-bsp"
execute_cmd "make -j8"
execute_cmd "pack"
